var Safe;
(function (Safe) {
    var Application;
    (function (Application) {
        var Sys = Safe.System;
        var dependencies = [
            'ngRoute',
            'ui.bootstrap',
            'smart-table',
            'bgf.paginateAnything', 'pageslide-directive', 'pascalprecht.translate'
        ];
        var mode = new Sys.DebugMode('app');
        mode.reportExceptions = false;
        Application.module = new Sys.Module(mode, dependencies);
        // const appComponentOptions: ng.IComponentOptions = {
        //     templateUrl: './views/sampleFormView.html'
        // };
        // export const module = angular
        //     .module('app', dependencies)
        //     .component('app', appComponentOptions);
        Application.module.config(configApp).run(runApp);
        //region config & run
        configApp.$inject = ['$httpProvider', '$compileProvider', '$routeProvider', '$locationProvider', '$translateProvider'];
        function configApp($httpProvider, $compileProvider, $routeProvider, $locationProvider, $translateProvider) {
            $locationProvider.html5Mode(true);
            $httpProvider.interceptors.push(Application.module.interceptorName);
            $compileProvider.debugInfoEnabled(true);
            $translateProvider.useMissingTranslationHandlerLog();
            $translateProvider.useStaticFilesLoader({
                prefix: '../i18n/employer/ApplicationResources_',
                suffix: '.json' // suffix, currently- extension of the translations
            });
            //$translateProvider.translations('en-us', translations);
            $translateProvider.preferredLanguage('en-us');
            //$translateProvider.useLocalStorage();
            var _this = this;
            //Translation Loading
            //Translation Loading Ends
            // insanely basic routing. Have fun.
            var firstView = true;
            _.each(Application.module.views, function (view) {
                var path = "/" + view.ngName;
                var route = {
                    templateUrl: view.templateUrl,
                    controllerAs: '$ctrl',
                    controller: view,
                };
                $routeProvider.when(path, route);
                if (firstView) {
                    $routeProvider.otherwise(route);
                    firstView = false;
                }
            });
        }
        runApp.$inject = [];
        function runApp() {
        }
        //endregion
    })(Application = Safe.Application || (Safe.Application = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=app.js.map