var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Product;
    (function (Product) {
        var Library;
        (function (Library) {
            var Test;
            (function (Test) {
                var DropDown = (function (_super) {
                    __extends(DropDown, _super);
                    function DropDown() {
                        var _this = _super.call(this) || this;
                        _this.classNameProp.setIf(['form-control']);
                        return _this;
                    }
                    return DropDown;
                }(Safe.DataControl));
                DropDown.ngName = 'safeDropDown';
                DropDown.exBindings = [
                    new Safe.StaticBinding('size', Safe.VarType.Number, 5).useSmartVar(),
                ];
                DropDown.template = "<label class=\"form-label\">{{$ctrl.displayText}}<select \n    id=\"{{$ctrl.id}}\" \n    name=\"$ctrl.name\" \n    ng-class=\"$ctrl.className\"\n    ng-model=\"$ctrl.ioValue\" \n    size=\"$ctrl.size\">\n        <option ng-repeat=\"row in $ctrl.inPut\">{{$ctrl.getDisplayText(row)}}</option>\n</select>\n</label>";
                Test.DropDown = DropDown;
            })(Test = Library.Test || (Library.Test = {}));
        })(Library = Product.Library || (Product.Library = {}));
    })(Product = Safe.Product || (Safe.Product = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=dropDown.js.map