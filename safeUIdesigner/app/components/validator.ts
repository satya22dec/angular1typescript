namespace Safe.Product.Library {

    export class ValidationControl extends InteractiveControl {
        constructor() {

            super();
            this.requiredProp.setIf(false);
           // alert(this.requiredProp.value);

        }
        static transclude = true;

        static ngName = 'safeValidate';

        static template=`
        <label>Validations added</label>
        <ng-transclude></ng-transclude>
        <p ng-repeat="row in $ctrl.validationObject">
            <span ng-show="row.isInvalidProp" class="error" >{{row.validationMessage}}</span>
        </p>
         `;

        static exBindings: Binding[] = [
            new StaticBinding('maxLength').useSmartVar(),
            new StaticBinding('minLength').useSmartVar(),
            new StaticBinding('required').useSmartVar()
          //  new StaticBinding('regex').useSmartVar()
        ];

        public readonly maxLengthProp: SmartString;
        public readonly minLengthProp: SmartString;
        public readonly requiredProp: SmartBool;
        private validationObject: {isInvalidProp: boolean, validationMessage: string}[];

        public $postLink() {
            // set for attribute
            super.$postLink();

            var self = this;
            this.validationObject = [];
            var i=0;
            _.each(this.getChildrenOfType<Input>(Input), (inputElement: Input) =>
            {
                $("#"+inputElement.idProp.value+" input").attr('pattern', ".{"+self.minLengthProp.value+","+ self.maxLengthProp.value+"}");

                self.validationObject.push({isInvalidProp: false, validationMessage: ''})
                var elem = <any> document.getElementById(inputElement.idProp.value).getElementsByTagName("input");

                //closure created for each click of an element
                function elementClick() {
                    var index = i;

                    $("#" + inputElement.idProp.value + " input").on('input', function (e) {

                        if (elem[0].checkValidity() == false) {
                         //   alert("triggered");
                            self.validationObject[index].isInvalidProp = true;
                           // errorFlag[index] = true;
                            self.validationObject[index].validationMessage = elem[0].validationMessage;
                        }
                        else {
                           // alert("trigerred 2");
                            self.validationObject[index].isInvalidProp = false;
                            self.validationObject[index].validationMessage= '';
                        }
                    });
                };
                elementClick();
               //alert(self.requiredProp.value);
                if(self.requiredProp.value){
                  //  alert(self.requiredProp.value);
                    elem[0].required = true;
                }

                if(elem[0].checkValidity()== false){

                    self.validationObject[i].isInvalidProp = true;
                    self.validationObject[i].validationMessage = elem[0].validationMessage;
                }
                i++;

            });


        }
    }
}