var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Product;
    (function (Product) {
        var Library;
        (function (Library) {
            var ValidationControl = (function (_super) {
                __extends(ValidationControl, _super);
                function ValidationControl() {
                    var _this = _super.call(this) || this;
                    _this.requiredProp.setIf(false);
                    return _this;
                    // alert(this.requiredProp.value);
                }
                ValidationControl.prototype.$postLink = function () {
                    // set for attribute
                    _super.prototype.$postLink.call(this);
                    var self = this;
                    this.validationObject = [];
                    var i = 0;
                    _.each(this.getChildrenOfType(Library.Input), function (inputElement) {
                        $("#" + inputElement.idProp.value + " input").attr('pattern', ".{" + self.minLengthProp.value + "," + self.maxLengthProp.value + "}");
                        self.validationObject.push({ isInvalidProp: false, validationMessage: '' });
                        var elem = document.getElementById(inputElement.idProp.value).getElementsByTagName("input");
                        //closure created for each click of an element
                        function elementClick() {
                            var index = i;
                            $("#" + inputElement.idProp.value + " input").on('input', function (e) {
                                if (elem[0].checkValidity() == false) {
                                    //   alert("triggered");
                                    self.validationObject[index].isInvalidProp = true;
                                    // errorFlag[index] = true;
                                    self.validationObject[index].validationMessage = elem[0].validationMessage;
                                }
                                else {
                                    // alert("trigerred 2");
                                    self.validationObject[index].isInvalidProp = false;
                                    self.validationObject[index].validationMessage = '';
                                }
                            });
                        }
                        ;
                        elementClick();
                        //alert(self.requiredProp.value);
                        if (self.requiredProp.value) {
                            //  alert(self.requiredProp.value);
                            elem[0].required = true;
                        }
                        if (elem[0].checkValidity() == false) {
                            self.validationObject[i].isInvalidProp = true;
                            self.validationObject[i].validationMessage = elem[0].validationMessage;
                        }
                        i++;
                    });
                };
                return ValidationControl;
            }(Safe.InteractiveControl));
            ValidationControl.transclude = true;
            ValidationControl.ngName = 'safeValidate';
            ValidationControl.template = "\n        <label>Validations added</label>\n        <ng-transclude></ng-transclude>\n        <p ng-repeat=\"row in $ctrl.validationObject\">\n            <span ng-show=\"row.isInvalidProp\" class=\"error\" >{{row.validationMessage}}</span>\n        </p>\n         ";
            ValidationControl.exBindings = [
                new Safe.StaticBinding('maxLength').useSmartVar(),
                new Safe.StaticBinding('minLength').useSmartVar(),
                new Safe.StaticBinding('required').useSmartVar()
                //  new StaticBinding('regex').useSmartVar()
            ];
            Library.ValidationControl = ValidationControl;
        })(Library = Product.Library || (Product.Library = {}));
    })(Product = Safe.Product || (Safe.Product = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=validator.js.map