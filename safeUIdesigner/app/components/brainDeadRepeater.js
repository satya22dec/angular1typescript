var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Product;
    (function (Product) {
        var Library;
        (function (Library) {
            var Test;
            (function (Test) {
                var Repeater = (function (_super) {
                    __extends(Repeater, _super);
                    function Repeater() {
                        return _super.call(this) || this;
                    }
                    return Repeater;
                }(Safe.Control));
                Repeater.ngName = 'safeRepeater';
                // TRY ME!! TURN OFF AND ON AND SEE!
                Repeater.transclude = true;
                // this dumb as donuts template just repeats what it got
                Repeater.template = "<div>\n    <p>I'm so dumb, all I can do is include the html from where I'm used. Here it is:</p>\n    <ng-transclude></ng-transclude>\n</div>\n";
                Test.Repeater = Repeater;
            })(Test = Library.Test || (Library.Test = {}));
        })(Library = Product.Library || (Product.Library = {}));
    })(Product = Safe.Product || (Safe.Product = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=brainDeadRepeater.js.map