namespace Safe.Product.Library.Test {
    export class DropDown extends DataControl {
        static ngName = 'safeDropDown';
        public constructor() {
            super();

            this.classNameProp.setIf(['form-control']);
        }

        static exBindings: Bindings = [
            new StaticBinding('size', VarType.Number, 5).useSmartVar(),
        ];

        //private size: number;
        public readonly sizeProp: SmartNumber;

        static template =
`<label class="form-label">{{$ctrl.displayText}}<select 
    id="{{$ctrl.id}}" 
    name="$ctrl.name" 
    ng-class="$ctrl.className"
    ng-model="$ctrl.ioValue" 
    size="$ctrl.size">
        <option ng-repeat="row in $ctrl.inPut">{{$ctrl.getDisplayText(row)}}</option>
</select>
</label>`;
    }
}