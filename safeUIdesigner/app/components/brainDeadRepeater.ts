namespace Safe.Product.Library.Test {
    export class Repeater extends Control {
        static ngName = 'safeRepeater';
        constructor() {
            super();
        }

        // TRY ME!! TURN OFF AND ON AND SEE!
        static transclude = true;

        // this dumb as donuts template just repeats what it got
        static template =
`<div>
    <p>I'm so dumb, all I can do is include the html from where I'm used. Here it is:</p>
    <ng-transclude></ng-transclude>
</div>
`;
    }
}