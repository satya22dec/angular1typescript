

namespace Safe.Application {
    import Sys = Safe.System;

    const dependencies: string[] = [
        'ngRoute',
        'ui.bootstrap',
        'smart-table',
        'bgf.paginateAnything', 'pageslide-directive', 'pascalprecht.translate'
    ];
    let mode = new Sys.DebugMode('app');
    mode.reportExceptions = false;

    export const module = new Sys.Module(mode, dependencies);
    // const appComponentOptions: ng.IComponentOptions = {
    //     templateUrl: './views/sampleFormView.html'
    // };
    // export const module = angular
    //     .module('app', dependencies)
    //     .component('app', appComponentOptions);
    module.config(configApp).run(runApp);

    //region config & run

    configApp.$inject = ['$httpProvider', '$compileProvider', '$routeProvider', '$locationProvider', '$translateProvider'];

    function configApp($httpProvider: ng.IHttpProvider,
        $compileProvider: ng.ICompileProvider,
        $routeProvider: angular.route.IRouteProvider,
        $locationProvider: ng.ILocationProvider,
        $translateProvider : ng.translate.ITranslateProvider) {
        $locationProvider.html5Mode(true);
        $httpProvider.interceptors.push(module.interceptorName);
        $compileProvider.debugInfoEnabled(true);
        $translateProvider.useMissingTranslationHandlerLog();
        $translateProvider.useStaticFilesLoader({
            prefix: '../i18n/employer/ApplicationResources_',// path to translations files
            suffix: '.json'// suffix, currently- extension of the translations
        });
        //$translateProvider.translations('en-us', translations);
        $translateProvider.preferredLanguage('en-us');
        //$translateProvider.useLocalStorage();
        let _this = this;
        //Translation Loading
        //Translation Loading Ends
        
        // insanely basic routing. Have fun.

        let firstView = true;
        _.each(module.views, view => {
            let path = `/${view.ngName}`;
            let route: angular.route.IRoute = {
                templateUrl: view.templateUrl,
                controllerAs: '$ctrl',
                controller: view,
            };
            $routeProvider.when(path, route);
            if (firstView) {
                $routeProvider.otherwise(route);
                firstView = false;
            }
        });

    }

    runApp.$inject = [];

    function runApp() {

    }

    //endregion

}

