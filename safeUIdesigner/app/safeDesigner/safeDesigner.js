var droppableField;
var ctrlDetails;
var html_flag = true;
var edit_element;
var editedUniqueKey = "";
var safeController = (function () {
    function safeController($window, $scope, $compile, $filter, apiService, htmlComponentService, $uibModal) {
        "ngInject";
        this.showMain = false;
        this.showDownload = false;
        this.hovering = false;
        this.showEditPanel = false;
        this.selectedFile = '1qa';
        this.activeTab = 3;
        this.dataSet = [];
        this.$window = $window;
        this.$scope = $scope;
        this.$compile = $compile;
        this.$filter = $filter;
        this.apiService = apiService;
        this.htmlComponentService = htmlComponentService;
        this.$uibModal = $uibModal;
        ctrlDetails = this;
    }
    safeController.prototype.$onInit = function () {
        ctrlDetails.apiService.getLibraryComponents().then(function (data) { return ctrlDetails.apiService.qsComponentData = data.config; });
        ctrlDetails.apiService.getFormDetails().then(function (data) { return ctrlDetails.apiService.safeFormData = data; });
    };
    /**
          called when all the changes needs to be reset.
        */
    safeController.prototype.resetChanges = function () {
        this.showMain = false;
        this.showDownload = false;
        this.selectedFile = "1qa";
        this.hovering = false;
        this.showEditPanel = false;
        this.activeTab = 3;
        document.getElementById('formToEdit').innerHTML = "";
        $("#file")[0].value = '';
        // this.$window.location.reload();
        // this.$location.path('/');
        // this.$location.path('/');
    };
    /**
          called when the template needs to be saved.
        */
    safeController.prototype.saveChanges = function () {
        this.showEditPanel = false;
        this.showDownload = true;
        this.showMain = false;
        this.fileName = this.selectedFile.slice(0, -5);
    };
    safeController.prototype.downloadTemplate = function () {
        // let componentList = ["safe-input", "safe-checkbox"];
        // let fileToSave1 = angular.copy(document.getElementById('formToEdit').innerHTML);
        // for (let value of componentList) {
        //     let elements: any = document.getElementsByTagName(value);
        //     for (let selected of elements) {
        //         selected.innerHTML = "";
        //     }
        // }
        var fileToSave = document.getElementById('hiddenInfo').innerHTML;
        var createFileBlob = new Blob([fileToSave], { type: 'text/html' }); //text/plain
        var blobFileURL = window.URL.createObjectURL(createFileBlob);
        var fileName = this.fileName;
        var link = angular.element('<a></a>');
        link.attr('download', fileName);
        link.attr('href', blobFileURL);
        document.body.appendChild(link[0]);
        link[0].click();
        window.URL.revokeObjectURL(blobFileURL);
        // document.getElementById('formToEdit').innerHTML = fileToSave1;
        this.resetChanges();
    };
    /**
          called when the html template is uploaded.
        */
    safeController.prototype.uploadFile = function (ev) {
        var _this = this;
        var targetFile = event.target;
        var file = targetFile.files[0];
        var reader = new FileReader();
        _this.showMain = true;
        _this.selectedFile = file.name;
        _this.dataSet = _this.apiService.safeFormData[0].dataSet;
        document.getElementById('formToEdit').innerHTML = "";
        reader.onload = function (event) {
            document.getElementById('hiddenInfo').innerHTML = event.target.result;
            angular.element(document.getElementById('formToEdit')).append(_this.$compile(event.target.result)(_this.$scope));
            _this.addDragNDrop();
        };
        reader.readAsText(file);
    };
    /*Drag and Drop features*/
    safeController.prototype.addDragNDrop = function () {
        var _this = this;
        var formElements = angular.element(document.getElementsByClassName('form-group'));
        angular.forEach(formElements, function (value) {
            angular.element(value).attr('draggable', 'true');
            angular.element(value).on('dragstart', _this.drag);
        });
        angular.element(document.getElementById('formToEdit')).unbind();
        angular.element(document.getElementById('formToEdit')).on('dragover', _this.allowDrop);
        angular.element(document.getElementById('formToEdit')).on('drop', _this.drop);
        _this.addHover();
    };
    ;
    safeController.prototype.addHover = function () {
        var _this = this;
        angular.element(document.getElementsByClassName('form-group')).on('mouseover', function () {
            _this.hoverField(this);
        });
        angular.element(document.getElementsByClassName('form-group')).on('mouseleave', function () {
            if (!_this.showEditPanel) {
                _this.removeHoverField(this);
            }
        });
    };
    //Show Field Actions
    safeController.prototype.hoverField = function (context) {
        var _this = this;
        if (!_this.hovering) {
            var ele = "<field-actions hovering='$ctrl.hovering' open-edit-panel='$ctrl.openEditPanel()' delete-ele='$ctrl.deleteEle' confirm-remove='$ctrl.confirmRemove()'></field-actions>";
            // let compileEle = angular.element(context).append(ele);
            // _this.$compile(compileEle)(_this.$scope);
            angular.element(context).append(_this.$compile(ele)(_this.$scope));
            _this.hovering = true;
        }
    };
    ;
    //Confirm remove field
    safeController.prototype.confirmRemove = function (ev) {
        var _this = ctrlDetails;
        _this.deleteEle = $(event.target).closest('.form-group');
        console.log("Deleting", _this.deleteEle);
        _this.$uibModal.open({
            backdrop: true,
            controller: ModalController,
            controllerAs: 'modal',
            templateUrl: 'app/safeDesigner/components/main/fieldActions/confirmModal.html',
            size: 'sm',
            resolve: {
                deleteEle: function () { return _this.deleteEle; }
            }
        });
    };
    //remove field actions
    safeController.prototype.removeHoverField = function (context) {
        var _this = this;
        _this.hovering = false;
        var myEl = angular.element(document.querySelector('field-actions'));
        myEl.remove();
    };
    ;
    //Edit Field Panel
    safeController.prototype.openEditPanel = function () {
        var _this = this;
        if (_this.showEditPanel) {
            event.preventDefault();
        }
        else {
            // _this.editEle = $(event.target).closest('.form-group');
            _this.editEle = $("#hiddenInfo #" + $(event.target).closest('.form-group')[0].id)[0];
            _this.editField();
            _this.showEditPanel = true;
        }
    };
    safeController.prototype.closeEditPanel = function () {
        var _this = this;
        _this.removeHoverField(this);
        _this.showEditPanel = false;
    };
    safeController.prototype.editField = function () {
        var _this = this;
        var selectEle = _this.apiService.qsComponentData;
        var nodeElem = angular.element(_this.editEle)[0].childNodes;
        html_flag = true;
        nodeElem.forEach(function (elem) {
            if (elem.tagName) {
                _this.apiService.qsComponentData.forEach(function (obj) {
                    if (elem.tagName === obj.fileName.toUpperCase()) {
                        var ioValue_1 = elem.getAttribute("io-Value");
                        _this.editFieldType = obj;
                        Object.keys(_this.editFieldType.properties).forEach(function (key) {
                            var retrieveVal;
                            if (key === "in-Put") {
                                editedUniqueKey = elem.getAttribute(key).substr(14);
                                retrieveVal = _this.dataSet[editedUniqueKey];
                            }
                            else {
                                retrieveVal = elem.getAttribute(key);
                            }
                            _this.editFieldType.properties[key] = retrieveVal;
                        });
                        _this.editFieldType.modelObject = {};
                        _this.apiService.safeFormData[0].model.some(function (mod) {
                            if (mod.modelName === ioValue_1) {
                                _this.editFieldType.modelObject = mod;
                            }
                            return mod.modelName === ioValue_1;
                        });
                        html_flag = false;
                        edit_element = elem;
                    }
                });
            }
        });
        if (html_flag) {
            angular.forEach(selectEle, function (value) {
                if ($(_this.editEle).has('input[type=text]').length) {
                    if (value.componentType == 'html-component' && value.component == 'input') {
                        _this.editFieldType = value;
                        _this.editFieldType.showInputEdit = true;
                        console.log("Input content:", $(_this.editEle).find('label').text());
                        _this.editFieldType.properties.labelName = $(_this.editEle).find('label').text();
                        _this.editFieldType.properties.inputId = $(_this.editEle).find('input')[0].id;
                        _this.editFieldType.properties.placeholder = $(_this.editEle).find('input')[0].placeholder;
                        if (true === $(_this.editEle).find('input')[0].required) {
                            _this.editFieldType.properties.required = true;
                        }
                    }
                }
                else if ($(_this.editEle).has('select').length) {
                    if (value.componentType == 'html-component' && value.component == 'dropdown') {
                        _this.editFieldType = value;
                        _this.editFieldType.properties.labelName = $(_this.editEle).find('label').text();
                        //_this.editFieldType.properties.default = $(_this.editEle).find('options')[0].default;
                        _this.editFieldType.properties.disabled = $(_this.editEle).find('select')[0].disabled;
                        if (true === $(_this.editEle).find('select')[0].required) {
                            _this.editFieldType.properties.required = true;
                        }
                    }
                }
                else if ($(_this.editEle).has('input[type=checkbox]').length) {
                    if (value.componentType == 'html-component' && value.component == 'checkbox') {
                        _this.editFieldType = value;
                        _this.editFieldType.properties.labelName = $(_this.editEle).find('label').text();
                        _this.editFieldType.properties.checked = $(_this.editEle).find('input')[0].checked;
                        _this.editFieldType.properties.disabled = $(_this.editEle).find('input')[0].disabled;
                    }
                }
                else if ($(_this.editEle).has('input[type=radio]').length) {
                    if (value.componentType == 'html-component' && value.component == 'radio') {
                        _this.editFieldType = value;
                        _this.editFieldType.properties.labelName = $(_this.editEle).find('label').text();
                        _this.editFieldType.properties.disabled = $(_this.editEle).find('input')[0].disabled;
                    }
                }
            });
            var ioValue_2 = $($(_this.editEle).find('input')[0]).attr("ng-model");
            _this.editFieldType.modelObject = {};
            _this.apiService.safeFormData[0].model.some(function (mod) {
                if (mod.modelName === ioValue_2) {
                    _this.editFieldType.modelObject = mod;
                    return true;
                }
                return false;
            });
        }
        _this.editFieldType.modelSet = _this.apiService.safeFormData[0].model;
        console.log("Finally Editing:", _this.editFieldType, _this.editFieldType.properties);
    };
    safeController.prototype.closeEditField = function () {
        var _this = this;
        _this.editEle = {};
        _this.editFieldType = {};
        console.log("Callback after editing field.", _this.editFieldType);
    };
    safeController.prototype.updateField = function () {
        var _this = this;
        var domElem = $("#formToEdit #" + $(_this.editEle)[0].id)[0];
        if (html_flag) {
            if ($(_this.editEle).has('input[type=text]').length) {
                console.log("Input content:", $(_this.editEle).find('label').text());
                $(_this.editEle).find('label')[0].innerText = _this.editFieldType.properties.labelName;
                $(_this.editEle).find('input')[0].id = _this.editFieldType.properties.inputId;
                $(_this.editEle).find('input')[0].placeholder = _this.editFieldType.properties.placeholder;
                $(_this.editEle).find('input')[0].required = _this.editFieldType.properties.required;
                _this.editFieldType.showInputEdit = false;
            }
            else if ($(_this.editEle).has('select').length) {
                console.log("Select content:", $(_this.editEle).find('label').text());
                $(_this.editEle).find('label')[0].innerText = _this.editFieldType.properties.labelName;
                //$(_this.editEle).find('select')[0].default = _this.editFieldType.properties.default;
                $(_this.editEle).find('select')[0].disabled = _this.editFieldType.properties.disabled;
                $(_this.editEle).find('select')[0].required = _this.editFieldType.properties.required;
                _this.editFieldType.showSelectEdit = false;
            }
            else if ($(_this.editEle).has('input[type=checkbox]').length) {
                $(_this.editEle).find('label')[0].innerText = _this.editFieldType.properties.labelName;
                $(_this.editEle).find('input')[0].checked = _this.editFieldType.properties.checked;
                $(_this.editEle).find('input')[0].disabled = _this.editFieldType.properties.disabled;
            }
            else if ($(_this.editEle).has('input[type=radio]').length) {
                $(_this.editEle).find('label')[0].innerText = _this.editFieldType.properties.labelName;
                $(_this.editEle).find('input')[0].disabled = _this.editFieldType.properties.disabled;
                _this.editFieldType.showSelectEdit = false;
            }
            $($(_this.editEle).find('input')[0]).attr("ng-model", _this.editFieldType.modelObject.modelName);
        }
        else {
            Object.keys(_this.editFieldType.properties).forEach(function (key) {
                if (key === "in-Put") {
                    _this.dataSet[editedUniqueKey] = _this.editFieldType.properties[key].split(",");
                }
                else {
                    $($(edit_element).attr(key, _this.editFieldType.properties[key]));
                }
            });
            $(edit_element).attr("io-Value", _this.editFieldType.modelObject.modelName);
            _this.$compile(edit_element)(_this.$scope);
        }
        $(domElem).html($($(_this.editEle)[0]).html());
        _this.$compile(domElem)(_this.$scope);
        _this.addDragNDrop();
        _this.closeEditPanel();
        _this.closeEditField();
    };
    //Delete Field
    safeController.prototype.allowDrop = function (ev) {
        ev.preventDefault();
    };
    safeController.prototype.drag = function (ev) {
        droppableField = event.target;
    };
    safeController.prototype.drop = function (ev) {
        ev.stopPropagation();
        ev.preventDefault();
        var data = droppableField;
        var _this = ctrlDetails;
        var uniqid = "safe" + Date.now();
        var componentUniqueInputModel = "safeComp" + Date.now();
        _this.dataSet[componentUniqueInputModel] = ['True', 'False'];
        if (data.tagName && data.tagName === "DIV" && droppableField.id === "qs-elements") {
            document.getElementById('qs-elements').style.left = event.x + "px";
            document.getElementById('qs-elements').style.top = event.y + "px";
            _this.addHover();
        }
        else if (data.tagName === "LI" && $(event.target).closest('.form-group').length > 0) {
            var elementDetails = JSON.parse(droppableField.dataset.id);
            var dataDetails = "";
            if (elementDetails.componentType === "html-component") {
                dataDetails = "<div class='form-group' id=" + uniqid + " draggable='true' onDragstart='angular.element(this).scope().$ctrl.drag(event)' >" + _this.htmlComponentService.getFieldDetails(elementDetails.component) + "</div>";
            }
            else {
                var componentDef = elementDetails.component;
                var compiledComponent = _this.$compile(componentDef)(_this.$scope);
                compiledComponent[0].setAttribute("in-Put", "$ctrl.dataSet." + componentUniqueInputModel);
                compiledComponent[0].innerHTML = "";
                dataDetails = "<div class='form-group' id=" + uniqid + " draggable='true' onDragstart='angular.element(this).scope().$ctrl.drag(event)' >" + compiledComponent[0].outerHTML + "</div>";
            }
            //  if ($(event.target).closest('.form-group').length > 0) {
            //     $($(event.target).closest('.form-group')[0]).before(_this.$compile(dataDetails)(_this.$scope));
            // } else if ($(event.target).closest('.form-heading').length > 0) {
            //     let modDataDetails = "<div class='col-sm-6 col-xs-12'>" + dataDetails + "</div>";
            //     $($(event.target).closest('.form-heading')[0]).append(_this.$compile(modDataDetails)(_this.$scope));
            // }
            var dropAdd = $("#hiddenInfo #" + $(event.target).closest('.form-group')[0].id)[0];
            if (dropAdd) {
                $(dropAdd).before(dataDetails);
            }
            $($(event.target).closest('.form-group')[0]).before(_this.$compile(dataDetails)(_this.$scope));
            _this.addHover();
        }
        else if (droppableField.id !== "qs-elements" && $(event.target).closest('.form-group').length > 0 && $(event.target).closest('.form-group')[0].id != $(droppableField).closest('.form-group')[0].id) {
            var dropDel = $("#hiddenInfo #" + $(droppableField).closest('.form-group')[0].id)[0];
            var dropAdd = $("#hiddenInfo #" + $(event.target).closest('.form-group')[0].id)[0];
            var data1 = dropDel;
            if (dropDel && dropAdd) {
                dropDel.parentNode.removeChild(dropDel);
                $(dropAdd).before(data1);
            }
            droppableField.parentNode.removeChild(droppableField);
            $($(event.target).closest('.form-group')[0]).before(data);
            // let newDom = _this.$compile(document.getElementById('hiddenInfo').innerHTML)(_this.$scope);
            // document.getElementById('formToEdit').innerHTML = "";
            // angular.element(document.getElementById('formToEdit')).append(newDom);
            // _this.addDragNDrop();
        }
    };
    return safeController;
}());
safeController.$inject = ['$window', '$scope', '$compile', '$filter', 'apiService', 'htmlComponentService', '$uibModal'];
/**
 * Modal controller
 */
var ModalController = (function () {
    function ModalController($uibModalInstance, deleteEle) {
        this.$uibModalInstance = $uibModalInstance;
        console.log("THis was passed", deleteEle);
        this.delEle = deleteEle;
    }
    ModalController.prototype.ok = function () {
        console.log("passed this to delete", this.delEle);
        $("#hiddenInfo #" + this.delEle[0].id)[0].remove();
        this.delEle.remove();
        this.$uibModalInstance.close();
    };
    ModalController.prototype.cancel = function () {
        this.$uibModalInstance.dismiss();
    };
    return ModalController;
}());
var safeDesigner = {
    controller: safeController,
    templateUrl: 'app/safeDesigner/safeDesigner.html'
};
angular.module('app').component('safeDesigner', safeDesigner);
//# sourceMappingURL=safeDesigner.js.map