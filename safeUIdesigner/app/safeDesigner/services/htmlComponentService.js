var HtmlComponentService = (function () {
    /**
             Returns draggable element details as per the component type
        */
    function HtmlComponentService($http, $q) {
        this.$http = $http;
        this.$q = $q;
        this.getFieldDetails = function (field) {
            var inp = "";
            if (field === 'input') {
                inp = "\n                    <label  class=\"form-label\"  >Default Label</label>\n                    <input type=\"text\"  placeholder=\"Placeholder\"  class=\"form-control\" name=\"test\" \">\n                    ";
            }
            else if (field === 'dropdown') {
                inp = "\n                    <label  class=\"form-label\" >Default Dropdown</label>\n                    <div class=\"select-wrap\">\n                        <select class=\"form-control\" autofocus   >\n                          <option value=\"\" >select options</option>\n                        </select>\n                      </div>\n                ";
            }
            else if (field === 'radio') {
                inp = "\n                    <input id=\"random\" type=\"radio\" >\n                    <label for=\"random\" class=\"form-label\" style=\"margin-left: 1em\">Is Default Selected ?</label>\n                ";
            }
            else {
                inp = "\n                    <input id=\"random\" type=\"checkbox\" >\n                    <label for=\"random\" class=\"form-label\" style=\"margin-left: 1em\">Is Default Selected ?</label>\n                ";
            }
            return inp;
            // return $interpolate(inp)({ field: field });
        };
    }
    return HtmlComponentService;
}());
angular.module('app').service('htmlComponentService', HtmlComponentService);
//# sourceMappingURL=htmlComponentService.js.map