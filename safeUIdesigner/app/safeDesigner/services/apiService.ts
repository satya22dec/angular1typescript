
class ApiService {

    constructor(private $http: ng.IHttpService, private $q: ng.IQService) {
    }
    qsComponentData: any;
    safeFormData: any;
    getLibraryComponents(): ng.IPromise<any[]> {
        return this.$q(resolve => {
            this.$http.get('./app/qsLibrary/library.config.json').then(response => {
                return resolve(response.data);
            });
        });
    }
    getFormDetails(): ng.IPromise<any[]> {
        return this.$q(resolve => {
            this.$http.get('./app/assets/data/employee_details.json').then(response => {
                return resolve(response.data);
            });
        });
    }
}

angular.module('app').service('apiService', ApiService);