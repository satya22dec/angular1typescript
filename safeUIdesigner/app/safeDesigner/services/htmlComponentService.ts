class HtmlComponentService {
    /**
             Returns draggable element details as per the component type
        */
    constructor(private $http: ng.IHttpService, private $q: ng.IQService) {
    }
    getFieldDetails = function (field: string) {
        let inp = "";
        if (field === 'input') {
            inp = `
                    <label  class="form-label"  >Default Label</label>
                    <input type="text"  placeholder="Placeholder"  class="form-control" name="test" ">
                    `;
        } else if (field === 'dropdown') {
            inp = `
                    <label  class="form-label" >Default Dropdown</label>
                    <div class="select-wrap">
                        <select class="form-control" autofocus   >
                          <option value="" >select options</option>
                        </select>
                      </div>
                `;
        } else if (field === 'radio') {
            inp = `
                    <input id="random" type="radio" >
                    <label for="random" class="form-label" style="margin-left: 1em">Is Default Selected ?</label>
                `;
        }
        else {
            inp = `
                    <input id="random" type="checkbox" >
                    <label for="random" class="form-label" style="margin-left: 1em">Is Default Selected ?</label>
                `;
        }
        return inp;
        // return $interpolate(inp)({ field: field });
    }
}
angular.module('app').service('htmlComponentService', HtmlComponentService);