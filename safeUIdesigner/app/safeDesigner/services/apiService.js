var ApiService = (function () {
    function ApiService($http, $q) {
        this.$http = $http;
        this.$q = $q;
    }
    ApiService.prototype.getLibraryComponents = function () {
        var _this = this;
        return this.$q(function (resolve) {
            _this.$http.get('./app/qsLibrary/library.config.json').then(function (response) {
                return resolve(response.data);
            });
        });
    };
    ApiService.prototype.getFormDetails = function () {
        var _this = this;
        return this.$q(function (resolve) {
            _this.$http.get('./app/assets/data/employee_details.json').then(function (response) {
                return resolve(response.data);
            });
        });
    };
    return ApiService;
}());
angular.module('app').service('apiService', ApiService);
//# sourceMappingURL=apiService.js.map