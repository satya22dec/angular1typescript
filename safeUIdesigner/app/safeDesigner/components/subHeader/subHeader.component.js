var subHeaderController = (function () {
    function subHeaderController($uibModal) {
        "ngInject";
        this.isFileEmpty = true;
        this.fileName = "wwwww";
        this.$uibModal = $uibModal;
    }
    subHeaderController.prototype.$onInit = function () {
    };
    subHeaderController.prototype.openDowmloadTemplate = function () {
        var _this = this;
        var modalInstance = _this.$uibModal.open({
            animation: false,
            component: "downloadTemplate",
            resolve: {
                fileName: function () {
                    return _this.fileName;
                }
            }
        });
        modalInstance.result.then(function (data) {
            console.log("data", data);
        });
    };
    return subHeaderController;
}());
subHeaderController.$inject = ['$uibModal'];
var subHeader = {
    bindings: {
        uploadFile: '&',
        saveChanges: '&',
        resetChanges: '&',
        selectedFile: '=',
        showMain: '='
    },
    controller: subHeaderController,
    templateUrl: 'app/safeDesigner/components/subHeader/subHeader.html'
};
angular.module('app').component('subHeader', subHeader);
//# sourceMappingURL=subHeader.component.js.map