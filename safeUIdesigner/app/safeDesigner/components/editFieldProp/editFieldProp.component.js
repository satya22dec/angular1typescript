var editFieldPropController = (function () {
    function editFieldPropController() {
        /* public static $inject = ['apiService']; */
        this.booleanFields = ['in-disabled', 'in-show'];
    }
    editFieldPropController.prototype.$onInit = function () {
    };
    return editFieldPropController;
}());
var editFieldProp = {
    bindings: {
        showEditPanel: '=',
        openEditPanel: '&',
        closeEditPanel: '&',
        editField: '&',
        closeEditField: '&',
        editFieldType: '=',
        updateField: '&'
    },
    controller: editFieldPropController,
    templateUrl: 'app/safeDesigner/components/editFieldProp/editFieldProp.html'
};
angular.module('app').component('editFieldProp', editFieldProp);
//# sourceMappingURL=editFieldProp.component.js.map