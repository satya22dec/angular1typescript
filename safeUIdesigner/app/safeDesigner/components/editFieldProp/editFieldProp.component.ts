
class editFieldPropController implements ng.IComponentController {
    /* public static $inject = ['apiService']; */
    booleanFields: any = ['in-disabled', 'in-show'];
    constructor() {
    }
    $onInit() {

    }

}
const editFieldProp: ng.IComponentOptions = {
    bindings: {
        showEditPanel: '=',
        openEditPanel: '&',
        closeEditPanel: '&',
        editField: '&',
        closeEditField: '&',
        editFieldType: '=',
        updateField: '&'
    },
    controller: editFieldPropController,
    templateUrl: 'app/safeDesigner/components/editFieldProp/editFieldProp.html'
};
angular.module('app').component('editFieldProp', editFieldProp);
