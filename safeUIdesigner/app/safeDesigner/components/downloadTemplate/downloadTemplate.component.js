var downloadTemplate = {
    bindings: {
        fileName: '=',
        downloadTemplate: '&'
    },
    templateUrl: 'app/safeDesigner/components/downloadTemplate/downloadTemplate.html'
};
angular.module('app').component('downloadTemplate', downloadTemplate);
//# sourceMappingURL=downloadTemplate.component.js.map