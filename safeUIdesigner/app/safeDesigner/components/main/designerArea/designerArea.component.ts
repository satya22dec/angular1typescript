
const designerArea: ng.IComponentOptions = {

    templateUrl: 'app/safeDesigner/components/main/designerArea/designerArea.html'
};
angular.module('app').component('designerArea', designerArea);
