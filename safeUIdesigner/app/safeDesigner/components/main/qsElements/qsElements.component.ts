
class qsElementsController implements ng.IComponentController {
    private apiService: any;
    public static $inject = ['apiService'];
    componentData: any;
    constructor(apiService: any) {
        "ngInject";
        this.apiService = apiService;
    }

    $onInit() {
        this.componentData = this.apiService.qsComponentData;
    }
    qsTabs = [{
        key: 'safe-component',
        title: "SAFE Components"
    }, {
        key: 'safe-widget',
        title: "SAFE Widgets"
    }, {
        key: 'html-component',
        title: "HTML Elements"
    }];
    onTabSelected(key) {

    };
}
const qsElements: ng.IComponentOptions = {
    bindings: {
        drag: '&',
        activeTab: "="
    },
    controller: qsElementsController,
    templateUrl: 'app/safeDesigner/components/main/qsElements/qsElements.html'
};
angular.module('app').component('qsElements', qsElements);
