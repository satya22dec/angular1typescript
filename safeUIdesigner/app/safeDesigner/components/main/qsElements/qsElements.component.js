var qsElementsController = (function () {
    function qsElementsController(apiService) {
        "ngInject";
        this.qsTabs = [{
                key: 'safe-component',
                title: "SAFE Components"
            }, {
                key: 'safe-widget',
                title: "SAFE Widgets"
            }, {
                key: 'html-component',
                title: "HTML Elements"
            }];
        this.apiService = apiService;
    }
    qsElementsController.prototype.$onInit = function () {
        this.componentData = this.apiService.qsComponentData;
    };
    qsElementsController.prototype.onTabSelected = function (key) {
    };
    ;
    return qsElementsController;
}());
qsElementsController.$inject = ['apiService'];
var qsElements = {
    bindings: {
        drag: '&',
        activeTab: "="
    },
    controller: qsElementsController,
    templateUrl: 'app/safeDesigner/components/main/qsElements/qsElements.html'
};
angular.module('app').component('qsElements', qsElements);
//# sourceMappingURL=qsElements.component.js.map