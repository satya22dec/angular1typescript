var mainController = (function () {
    function mainController() {
        "ngInject";
        this.inputBoxEmployeeOne = {
            name: "EMPLOYEE INPUTBOXCOMPONENT"
        };
        this.inputBoxEmployeeTwo = {
            name: "PRODUCT INPUTBOXCOMPONENT"
        };
        this.sectionHeaderName = "Header";
    }
    mainController.prototype.$onInit = function () {
    };
    return mainController;
}());
mainController.$inject = [];
var main = {
    bindings: {
        drag: "&",
        activeTab: "="
    },
    controller: mainController,
    templateUrl: 'app/safeDesigner/components/main/main.html'
};
angular.module('app').component('main', main);
//# sourceMappingURL=main.component.js.map