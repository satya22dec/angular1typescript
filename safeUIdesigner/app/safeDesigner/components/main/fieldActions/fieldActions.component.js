var fieldActionsController = (function () {
    /* public static $inject = ['apiService']; */
    function fieldActionsController() {
    }
    fieldActionsController.prototype.$onInit = function () {
    };
    return fieldActionsController;
}());
var fieldActions = {
    bindings: {
        hovering: '=',
        showEditPanel: '=',
        openEditPanel: '&',
        deleteEle: '=',
        confirmRemove: '&'
    },
    controller: fieldActionsController,
    templateUrl: 'app/safeDesigner/components/main/fieldActions/fieldActions.html'
};
angular.module('app').component('fieldActions', fieldActions);
//# sourceMappingURL=fieldActions.component.js.map