
class fieldActionsController implements ng.IComponentController {
    /* public static $inject = ['apiService']; */
    constructor() {
    }
    $onInit() {

    }


    /* 
     closeEdit(){
         console.log("closing edit");
         angular.element(document.getElementById("editPanel")).style.width = "0";
     } */

}
const fieldActions: ng.IComponentOptions = {
    bindings: {
        hovering: '=',
        showEditPanel: '=',
       openEditPanel: '&',
        deleteEle: '=',
        confirmRemove: '&'
    },
    controller: fieldActionsController,
    templateUrl: 'app/safeDesigner/components/main/fieldActions/fieldActions.html'
};
angular.module('app').component('fieldActions', fieldActions);
