

class mainController implements ng.IComponentController {
    show: boolean;
    public static $inject = [];
    constructor() {
        "ngInject";
    }
    $onInit() {
    }
    inputBoxEmployeeOne: Object = {
        name: "EMPLOYEE INPUTBOXCOMPONENT"
    }
    inputBoxEmployeeTwo: Object = {
        name: "PRODUCT INPUTBOXCOMPONENT"
    }
    sectionHeaderName: string = "Header";
}
const main: ng.IComponentOptions = {
    bindings: {
        drag: "&",
        activeTab: "="
    },
    controller: mainController,
    templateUrl: 'app/safeDesigner/components/main/main.html'
};
angular.module('app').component('main', main);
