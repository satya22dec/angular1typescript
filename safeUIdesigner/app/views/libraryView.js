var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Application;
    (function (Application) {
        var LibraryView = (function (_super) {
            __extends(LibraryView, _super);
            function LibraryView(safeModule) {
                var _this = _super.call(this) || this;
                _this.safeModule = safeModule;
                _this.library = [];
                var library = safeModule.componentMaps.library;
                _.each(_.keys(library), function (ngName) {
                    var componentClass = library[ngName];
                    _this.library.push({
                        ngName: ngName,
                        tagName: ngName.dasherize(),
                        tsName: componentClass.name,
                        numBindings: componentClass.GetAllBindings().length,
                    });
                });
                return _this;
            }
            return LibraryView;
        }(Safe.View));
        LibraryView.ngName = 'libraryView';
        LibraryView.templateUrl = 'app/views/libraryView.html';
        LibraryView.$inject = ['safeModule'];
        Application.LibraryView = LibraryView;
    })(Application = Safe.Application || (Safe.Application = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=libraryView.js.map