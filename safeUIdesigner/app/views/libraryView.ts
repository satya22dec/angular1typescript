namespace Safe.Application {
    import SS = Safe.System;
    export class LibraryView extends View {
        static ngName = 'libraryView';
        static templateUrl = 'app/views/libraryView.html';
        static $inject: string[] = ['safeModule'];

        constructor(private safeModule: SS.Module) {
            super();

            let library = safeModule.componentMaps.library;
            _.each(_.keys(library), ngName => {
                let componentClass = library[ngName];
                this.library.push({
                    ngName: ngName,
                    tagName: ngName.dasherize(),
                    tsName: componentClass.name,
                    numBindings: componentClass.GetAllBindings().length,
                });
            });
        }

        private library: any[] = [];
    }

}
