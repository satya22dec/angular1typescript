namespace Safe.Application {
    export class DesignerView extends View {
        static ngName = 'designerView';
        static templateUrl = 'app/views/designerView.html';
        static $inject: string[] = [];
        constructor() {
            super();

        }
    }
}
