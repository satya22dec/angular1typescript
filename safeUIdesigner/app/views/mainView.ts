namespace Safe.Application {
    export class MainView extends View {
        static ngName = 'mainView';
        static templateUrl = 'app/views/mainView.html';
        static $inject: string[] = [];
        constructor() {
            super();

        }

        private url = 'https://jsonplaceholder.typicode.com/users';
        private myName = 'David';
        private data: AnyObj = null;
    }
}
