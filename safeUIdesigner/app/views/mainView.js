var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Application;
    (function (Application) {
        var MainView = (function (_super) {
            __extends(MainView, _super);
            function MainView() {
                var _this = _super.call(this) || this;
                _this.url = 'https://jsonplaceholder.typicode.com/users';
                _this.myName = 'David';
                _this.data = null;
                return _this;
            }
            return MainView;
        }(Safe.View));
        MainView.ngName = 'mainView';
        MainView.templateUrl = 'app/views/mainView.html';
        MainView.$inject = [];
        Application.MainView = MainView;
    })(Application = Safe.Application || (Safe.Application = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=mainView.js.map