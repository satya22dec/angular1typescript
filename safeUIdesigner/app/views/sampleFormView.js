var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Application;
    (function (Application) {
        var SampleFormView = (function (_super) {
            __extends(SampleFormView, _super);
            function SampleFormView() {
                var _this = _super.call(this) || this;
                _this.data = ['Is Private', 'Is Not Private'];
                return _this;
            }
            return SampleFormView;
        }(Safe.View));
        SampleFormView.ngName = 'sampleFormView';
        SampleFormView.templateUrl = '/app/views/sampleFormView.html';
        SampleFormView.$inject = [];
        Application.SampleFormView = SampleFormView;
    })(Application = Safe.Application || (Safe.Application = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=sampleFormView.js.map