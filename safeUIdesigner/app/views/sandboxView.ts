namespace Safe.Application {
    export class SandboxView extends View {
        static ngName = 'sandboxView';
        static templateUrl = '/app/views/sandboxView.html';
        static $inject: string[] = [];

        constructor() {
            super();

        }

        public click() {
            debugger;
        }

        private data: string[] = ['One', 'Two', 'Three', 'Four'];
        private choice: string;
        private checkedVal: any = 'YES';
        private name='David';
    }
}
