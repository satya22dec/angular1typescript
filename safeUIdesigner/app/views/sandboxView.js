var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Application;
    (function (Application) {
        var SandboxView = (function (_super) {
            __extends(SandboxView, _super);
            function SandboxView() {
                var _this = _super.call(this) || this;
                _this.data = ['One', 'Two', 'Three', 'Four'];
                _this.checkedVal = 'YES';
                _this.name = 'David';
                return _this;
            }
            SandboxView.prototype.click = function () {
                debugger;
            };
            return SandboxView;
        }(Safe.View));
        SandboxView.ngName = 'sandboxView';
        SandboxView.templateUrl = '/app/views/sandboxView.html';
        SandboxView.$inject = [];
        Application.SandboxView = SandboxView;
    })(Application = Safe.Application || (Safe.Application = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=sandboxView.js.map