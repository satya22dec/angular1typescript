var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Application;
    (function (Application) {
        var DesignerView = (function (_super) {
            __extends(DesignerView, _super);
            function DesignerView() {
                return _super.call(this) || this;
            }
            return DesignerView;
        }(Safe.View));
        DesignerView.ngName = 'designerView';
        DesignerView.templateUrl = 'app/views/designerView.html';
        DesignerView.$inject = [];
        Application.DesignerView = DesignerView;
    })(Application = Safe.Application || (Safe.Application = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=designerView.js.map