import * as e from 'express';

const express = require('express');
const app = express();
let path = require('path');

app.use(express.static(__dirname + '/'));

app.get('*', function (req: e.Request, res: e.Response) {
    console.log(`Path not found: ${req.url}. Returning index.html`);
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.listen(3535, function () {
    console.log('=> Safe Playground running on port 3535!');
    console.log('=> Open a browser and navigate to http://localhost:3535/ to see the component playground');
    console.log('=> Ctrl-C to quit the server');
});