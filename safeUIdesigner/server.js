"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var app = express();
var path = require('path');
app.use(express.static(__dirname + '/'));
app.get('*', function (req, res) {
    console.log("Path not found: " + req.url + ". Returning index.html");
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.listen(3535, function () {
    console.log('=> Safe Playground running on port 3535!');
    console.log('=> Open a browser and navigate to http://localhost:3535/ to see the component playground');
    console.log('=> Ctrl-C to quit the server');
});
//# sourceMappingURL=server.js.map