var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    // TODO: Refactor regions into files....
    //endregion
    var VarType;
    (function (VarType) {
        VarType[VarType["Unknown"] = 0] = "Unknown";
        VarType[VarType["Null"] = 1] = "Null";
        VarType[VarType["Any"] = 2] = "Any";
        VarType[VarType["Function"] = 3] = "Function";
        VarType[VarType["String"] = 4] = "String";
        VarType[VarType["Date"] = 5] = "Date";
        VarType[VarType["Time"] = 6] = "Time";
        VarType[VarType["DateTime"] = 7] = "DateTime";
        VarType[VarType["Percentage"] = 8] = "Percentage";
        VarType[VarType["Currency"] = 9] = "Currency";
        VarType[VarType["Boolean"] = 10] = "Boolean";
        VarType[VarType["Number"] = 11] = "Number";
        VarType[VarType["WholeNumber"] = 12] = "WholeNumber";
        VarType[VarType["RationalNumber"] = 13] = "RationalNumber";
        VarType[VarType["Object"] = 14] = "Object";
        VarType[VarType["CompoundObject"] = 15] = "CompoundObject";
        VarType[VarType["SimpleObject"] = 16] = "SimpleObject";
        VarType[VarType["Array"] = 17] = "Array";
        VarType[VarType["Bag"] = 18] = "Bag";
        VarType[VarType["Table"] = 19] = "Table";
        VarType[VarType["Component"] = 20] = "Component";
    })(VarType = Safe.VarType || (Safe.VarType = {}));
    /** our extended binding type */
    var Binding = (function () {
        function Binding(name, type, defaultValue, required) {
            this.name = name;
            this.type = type;
            this.defaultValue = defaultValue;
            this.required = required;
            this.__S_smart = false;
            // this neat thing makes sure I can check for a Binding type if/when it's passed - see SmartVar ctor
            this.bindingMarker = this;
        }
        Binding.isBinding = function (obj) {
            return _.isObject(obj) && !_.isUndefined(obj.bindingMarker) && obj.bindingMarker instanceof Binding;
        };
        Binding.prototype.useSmartVar = function () {
            this.__S_smart = true;
            return this;
        };
        Object.defineProperty(Binding.prototype, "smartVarEnabled", {
            get: function () {
                return this.__S_smart;
            },
            enumerable: true,
            configurable: true
        });
        Binding.prototype.toString = function () {
            return "Binding " + this.constructor.name + "('" + this.name + ")')";
        };
        Binding.ToNgBinding = function (binding) {
            var bindings = _.isArray(binding) ? binding : [binding];
            var ret = {};
            _.each(bindings, function (binding) {
                ret[binding.name] = binding.constructor['ngSymbol'] + (binding.required ? '' : '?');
            });
            return ret;
        };
        return Binding;
    }());
    Safe.Binding = Binding;
    var NullBinding = (function (_super) {
        __extends(NullBinding, _super);
        function NullBinding(name, type, defaultValue, required) {
            if (name === void 0) { name = 'val'; }
            if (type === void 0) { type = VarType.Unknown; }
            if (required === void 0) { required = false; }
            var _this = _super.call(this, name, type, defaultValue, required) || this;
            _this.name = name;
            _this.type = type;
            _this.defaultValue = defaultValue;
            _this.required = required;
            return _this;
        }
        NullBinding.prototype.useSmartVar = function () {
            throw new Safe.System.FoundationException('Cannot use smart variable for NullBinding');
        };
        return NullBinding;
    }(Binding));
    NullBinding.ngSymbol = '!!BAADF00D!!';
    Safe.NullBinding = NullBinding;
    var StaticBinding = (function (_super) {
        __extends(StaticBinding, _super);
        function StaticBinding(name, type, defaultValue, required) {
            if (type === void 0) { type = VarType.String; }
            if (required === void 0) { required = false; }
            var _this = _super.call(this, name, type, defaultValue, required) || this;
            _this.name = name;
            _this.type = type;
            _this.defaultValue = defaultValue;
            _this.required = required;
            if (name.indexOf('in') === 0 ||
                name.indexOf('io') === 0 ||
                name.indexOf('out') === 0 ||
                name.indexOf('on') === 0) {
                throw new Safe.System.FoundationException("Static binding name must NOT start with 'in', 'io', 'out', or 'on'. " + _this.toString() + "\"");
            }
            return _this;
        }
        return StaticBinding;
    }(Binding));
    StaticBinding.ngSymbol = '@';
    Safe.StaticBinding = StaticBinding;
    var InputBinding = (function (_super) {
        __extends(InputBinding, _super);
        function InputBinding(name, type, defaultValue, required) {
            if (type === void 0) { type = VarType.Unknown; }
            if (required === void 0) { required = false; }
            var _this = _super.call(this, name, type, defaultValue, required) || this;
            _this.name = name;
            _this.type = type;
            _this.defaultValue = defaultValue;
            _this.required = required;
            if (name.indexOf('in') != 0) {
                throw new Safe.System.FoundationException("Input binding name must start with 'in'. " + _this.toString() + "\"");
            }
            return _this;
        }
        return InputBinding;
    }(Binding));
    InputBinding.ngSymbol = '<';
    Safe.InputBinding = InputBinding;
    var TwoWayBinding = (function (_super) {
        __extends(TwoWayBinding, _super);
        function TwoWayBinding(name, type, defaultValue, required) {
            if (type === void 0) { type = VarType.Unknown; }
            if (required === void 0) { required = false; }
            var _this = _super.call(this, name, type, defaultValue, required) || this;
            _this.name = name;
            _this.type = type;
            _this.defaultValue = defaultValue;
            _this.required = required;
            if (!(name.indexOf('out') !== 0 || name.indexOf('io') !== 0)) {
                throw new Safe.System.FoundationException("Two-way binding name must start with 'out' or 'io'. " + _this.toString() + "\"");
            }
            return _this;
        }
        return TwoWayBinding;
    }(Binding));
    TwoWayBinding.ngSymbol = '=';
    Safe.TwoWayBinding = TwoWayBinding;
    var CallbackBinding = (function (_super) {
        __extends(CallbackBinding, _super);
        function CallbackBinding(name, type, defaultValue, required) {
            if (type === void 0) { type = VarType.Function; }
            if (defaultValue === void 0) { defaultValue = null; }
            if (required === void 0) { required = false; }
            var _this = _super.call(this, name, type, defaultValue, required) || this;
            _this.name = name;
            _this.type = type;
            _this.defaultValue = defaultValue;
            _this.required = required;
            if (name.indexOf('on') != 0) {
                throw new Safe.System.FoundationException("Callback binding name must start with 'on'. " + _this.toString() + "\"");
            }
            if (name !== 'onEvent') {
                console.warn('====== SUSPICIOUS CALLBACK BINDING USE ======');
                console.warn("====== " + name + " ======");
                console.warn('>> ARE YOU SURE TWO-WAY BINDING ISN\'T BETTER??');
                console.warn('>> ARE YOU SURE USING ONEVENT WON\'T DO??');
                console.warn('====== SUSPICIOUS CALLBACK BINDING USE ======');
            }
            return _this;
        }
        return CallbackBinding;
    }(Binding));
    CallbackBinding.ngSymbol = '&';
    Safe.CallbackBinding = CallbackBinding;
    //endregion
})(Safe || (Safe = {}));
//# sourceMappingURL=aaa_bindings.js.map