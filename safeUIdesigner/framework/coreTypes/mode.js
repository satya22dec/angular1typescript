var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var System;
    (function (System) {
        var Mode = (function () {
            function Mode(appName) {
                this.appName = appName;
                this.reportExceptions = true;
            }
            return Mode;
        }());
        System.Mode = Mode;
        var DebugMode = (function (_super) {
            __extends(DebugMode, _super);
            function DebugMode(appName) {
                return _super.call(this, appName) || this;
            }
            return DebugMode;
        }(Mode));
        System.DebugMode = DebugMode;
        var ProductionMode = (function (_super) {
            __extends(ProductionMode, _super);
            function ProductionMode(appName) {
                return _super.call(this, appName) || this;
            }
            return ProductionMode;
        }(Mode));
        System.ProductionMode = ProductionMode;
    })(System = Safe.System || (Safe.System = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=mode.js.map