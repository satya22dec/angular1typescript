var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var __gNullBinding = new Safe.NullBinding();
    //region SmartVar support
    var SmartFormatFlags;
    (function (SmartFormatFlags) {
        SmartFormatFlags[SmartFormatFlags["None"] = 0] = "None";
        SmartFormatFlags[SmartFormatFlags["Short"] = 1] = "Short";
        SmartFormatFlags[SmartFormatFlags["Medium"] = 2] = "Medium";
        SmartFormatFlags[SmartFormatFlags["Long"] = 4] = "Long";
        SmartFormatFlags[SmartFormatFlags["Whole"] = 256] = "Whole";
        SmartFormatFlags[SmartFormatFlags["DateOnly"] = 512] = "DateOnly";
        SmartFormatFlags[SmartFormatFlags["TimeOnly"] = 1024] = "TimeOnly";
        SmartFormatFlags[SmartFormatFlags["Default"] = 0] = "Default";
    })(SmartFormatFlags = Safe.SmartFormatFlags || (Safe.SmartFormatFlags = {}));
    var SmartVar = (function () {
        // TODO: Kinda ugly to accept any as the param - but this type's raison d'etre is to wrap an existing property, so...
        function SmartVar(init, boundObj) {
            this.boundTo = boundObj || this;
            this.binding = __gNullBinding;
            if (Safe.Binding.isBinding(init)) {
                this.binding = init; // being bound to property
            }
            else {
                this.set(init); // using stand alone, using initial value
            }
            if (this.binding.required && this.isUndefined) {
                throw new Safe.System.FoundationException("Missing required binding. " + this.binding.toString());
            }
            this.initialValue = this.value;
        }
        SmartVar.BindTo = function (binding, obj) {
            // TODO: Throw exception if the provided type mismatches the data value type.
            if (!(obj && binding)) {
                throw new Safe.System.FoundationException("Cannot create a factory SmartVar without object binding information");
            }
            var currentVal = obj[binding.name];
            if (Safe.isSmartVar(currentVal)) {
                return currentVal;
            }
            // create a new property on bound object iff it's not there already
            if (_.isUndefined(currentVal)) {
                Object.defineProperty(obj, binding.name, {
                    value: binding.defaultValue,
                    configurable: true,
                    writable: true,
                    enumerable: true
                });
                currentVal = binding.defaultValue;
            }
            var smartVar;
            if (binding instanceof Safe.CallbackBinding) {
                if (binding.defaultValue) {
                    throw new Safe.System.FoundationException("Cannot have default value for callback binding. " + binding.toString());
                }
                smartVar = new SmartCall(binding, obj);
            }
            else if (_.isObject(binding.type)) {
                smartVar = new SmartEnum(binding.type, binding, obj);
            }
            else {
                switch (binding.type) {
                    case Safe.VarType.Date:
                    case Safe.VarType.Time:
                    case Safe.VarType.DateTime:
                        smartVar = new SmartDate(binding, obj);
                        break;
                    case Safe.VarType.Number:
                    case Safe.VarType.Currency:
                    case Safe.VarType.RationalNumber:
                    case Safe.VarType.WholeNumber:
                        smartVar = new SmartNumber(binding, obj);
                        break;
                    case Safe.VarType.Boolean:
                        smartVar = new SmartBool(binding, obj);
                        break;
                    case Safe.VarType.Object:
                    case Safe.VarType.SimpleObject:
                    case Safe.VarType.CompoundObject:
                        smartVar = new SmartObj(binding, obj);
                        break;
                    case Safe.VarType.String:
                        smartVar = new SmartString(binding, obj);
                        break;
                    case Safe.VarType.Any:
                        smartVar = new SmartVar(binding, obj);
                        break;
                    case Safe.VarType.Array:
                    case Safe.VarType.Table: // array of objects
                    case Safe.VarType.Bag:
                        smartVar = new SmartArray(binding, obj);
                        break;
                    case Safe.VarType.Component:
                        smartVar = new SmartRef(binding, obj);
                        break;
                    default:
                        // discover
                        if (_.isString(currentVal))
                            smartVar = new SmartString(binding, obj);
                        else if (_.isNumber(currentVal))
                            smartVar = new SmartNumber(binding, obj);
                        else if (_.isDate(currentVal))
                            smartVar = new SmartDate(binding, obj);
                        else if (_.isBoolean(currentVal))
                            smartVar = new SmartBool(binding, obj);
                        else if (_.isArray(currentVal))
                            smartVar = new SmartArray(binding, obj);
                        else if (_.isObject(currentVal))
                            smartVar = new SmartObj(binding, obj);
                        break;
                }
            }
            if (!smartVar) {
                console.warn("Unable to find appropriate SmartVar for " + binding.toString() + ". Using SmartAny");
                smartVar = new SmartVar(binding, obj);
            }
            // and, finally, create the proxy object on the bound object
            obj[binding.name + "Prop"] = smartVar;
            return smartVar;
        };
        Object.defineProperty(SmartVar.prototype, "value", {
            get: function () {
                return this.preGet(this.boundTo[this.binding.name]);
            },
            enumerable: true,
            configurable: true
        });
        SmartVar.prototype.preGet = function (currentVal) {
            return currentVal;
        };
        SmartVar.prototype.preSet = function (newVal) {
            return newVal;
        };
        SmartVar.prototype.toString = function (fmt) {
            return this.hasValue ? this.value.toString() : '';
        };
        Object.defineProperty(SmartVar.prototype, "initialValue", {
            get: function () {
                return this._initialValue;
            },
            set: function (newVal) {
                this._initialValue = newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SmartVar.prototype, "ngValue", {
            // private for HTML/markup use only
            get: function () {
                return this.value;
            },
            set: function (newVal) {
                this.set(newVal);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SmartVar.prototype, "forMarkup", {
            // region helpers
            get: function () {
                return this.toString();
            },
            enumerable: true,
            configurable: true
        });
        SmartVar.prototype.eq = function (other) {
            return this.value === Safe.smartVal(other);
        };
        SmartVar.prototype.neq = function (other) {
            return !this.eq(other);
        };
        SmartVar.prototype.set = function (newVal) {
            if (_.isUndefined(newVal)) {
                if (this.binding.required) {
                    throw new Safe.System.FoundationException("Attempt to clear required bound value. " + this.binding.toString());
                }
                this.boundTo[this.binding.name] = newVal;
            }
            else {
                var preppedValue = Safe.isSmartVar(newVal) ? this.preSet(newVal.value) : this.preSet(newVal);
                this.boundTo[this.binding.name] = preppedValue;
            }
            return this;
        };
        /** Default is set if undefined */
        SmartVar.prototype.setIf = function (newVal, predicate) {
            if (predicate === void 0) { predicate = function (sv) { return sv.isUndefined; }; }
            if (predicate(this)) {
                this.set(newVal);
            }
            return this;
        };
        /* Default is throw if undefined */
        SmartVar.prototype.assert = function (truth) {
            if (_.isUndefined(truth)) {
                if (this.isNullOrUndefined) {
                    throw new Safe.System.AssertionException();
                }
            }
            else if (_.isFunction(truth) && !truth(this)) {
                throw new Safe.System.AssertionException();
            }
            else if (Safe.isSmartVar(truth) && this.value !== truth.value) {
                throw new Safe.System.AssertionException();
            }
            else if (this.value !== truth.valueOf()) {
                throw new Safe.System.AssertionException();
            }
            return this;
        };
        SmartVar.prototype.valueOf = function () {
            return this.value;
        };
        Object.defineProperty(SmartVar.prototype, "isDirty", {
            get: function () {
                return this.value !== this.initialValue;
            },
            enumerable: true,
            configurable: true
        });
        SmartVar.prototype.clean = function () {
            this.initialValue = this.value;
            return this;
        };
        SmartVar.prototype.reset = function () {
            this.set(this.initialValue);
            return this;
        };
        SmartVar.prototype.clear = function () {
            this.set();
            return this;
        };
        Object.defineProperty(SmartVar.prototype, "isNull", {
            get: function () {
                return this.value === null;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SmartVar.prototype, "isUndefined", {
            get: function () {
                return _.isUndefined(this.value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SmartVar.prototype, "isNullOrUndefined", {
            get: function () {
                return this.isUndefined || this.isNull;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SmartVar.prototype, "isEmpty", {
            get: function () {
                return _.isEmpty(this.value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SmartVar.prototype, "hasValue", {
            get: function () {
                return !this.isNullOrUndefined;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SmartVar.prototype, "watchExpression", {
            get: function () {
                return "$ctrl." + this.binding.name;
            },
            enumerable: true,
            configurable: true
        });
        SmartVar.prototype.watch = function () {
            if (this.boundTo === this || !(this.boundTo instanceof Safe.Component)) {
                throw new Safe.System.FoundationException('Cannot watch SmartVar not bound to a component');
            }
            var boundObj = this.boundTo;
            var scope = boundObj['$scope'];
            if (!scope) {
                throw new Safe.System.FoundationException('Cannot watch SmartVar outside of component scope');
            }
            var exp = this.watchExpression;
            if (!_.any(scope.$$watchers, function (w) { return _.isString(w.exp) && w.exp.toUpperCase() === exp.toUpperCase(); })) {
                scope.$watch(exp);
            }
            return this;
        };
        /** Combines watch and set (if undefined) */
        SmartVar.prototype.poke = function (val) {
            return this.isUndefined ? this.watch().set(val) : this;
        };
        return SmartVar;
    }());
    Safe.SmartVar = SmartVar;
    var SmartCall = (function (_super) {
        __extends(SmartCall, _super);
        function SmartCall(init, boundObj) {
            return _super.call(this, init, boundObj) || this;
        }
        SmartCall.prototype.invoke = function (arg) {
            if (this.hasValue) {
                // create the funk-o-matic params object that ng luvs
                var params = {};
                params[this.binding.name.toLowerCase().slice(2)] = arg;
                this._retVal = this.value(params);
                return this.retVal;
            }
            return undefined;
        };
        SmartCall.prototype.preSet = function (newVal) {
            throw new Safe.System.FoundationException("Attempt to set " + newVal + " on callback bound value. " + this.binding.toString());
        };
        Object.defineProperty(SmartCall.prototype, "retVal", {
            get: function () {
                return this._retVal;
            },
            enumerable: true,
            configurable: true
        });
        return SmartCall;
    }(SmartVar));
    Safe.SmartCall = SmartCall;
    var SmartBool = (function (_super) {
        __extends(SmartBool, _super);
        function SmartBool(init, boundObj) {
            return _super.call(this, init, boundObj) || this;
        }
        SmartBool.prototype.preSet = function (newVal) {
            return !!newVal;
        };
        SmartBool.prototype.toggle = function () {
            this.set(!this.value);
            return this;
        };
        Object.defineProperty(SmartBool.prototype, "on", {
            get: function () {
                return this.value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SmartBool.prototype, "off", {
            get: function () {
                return this.value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SmartBool.prototype, "yes", {
            get: function () {
                return this.on;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SmartBool.prototype, "no", {
            get: function () {
                return this.off;
            },
            enumerable: true,
            configurable: true
        });
        return SmartBool;
    }(SmartVar));
    Safe.SmartBool = SmartBool;
    var SmartDate = (function (_super) {
        __extends(SmartDate, _super);
        function SmartDate(init, boundObj) {
            var _this = _super.call(this, init, boundObj) || this;
            _this.setIf(moment());
            return _this;
        }
        SmartDate.prototype.preGet = function (currentVal) {
            return currentVal.clone();
        };
        SmartDate.prototype.preSet = function (newVal) {
            var m = null;
            if (moment.isMoment(newVal)) {
                m = newVal.clone();
            }
            else if (_.isDate(newVal)) {
                m = moment(newVal);
            }
            else if (_.isString(newVal)) {
                // TODO: Warn?
                m = moment(newVal);
                if (!m.isValid()) {
                    m = null;
                }
            }
            if (m) {
                if (this.binding.type === Safe.VarType.Date) {
                    m.startOf('d'); // mutates the object
                }
                return m;
            }
            throw new Safe.System.FoundationException("Attempt to set invalid date on bound value. " + this.binding.toString());
        };
        SmartDate.prototype.toString = function (fmt) {
            return this.format(fmt);
        };
        // TODO: Formatting ... ugh
        SmartDate.prototype.format = function (fmt) {
            if (fmt === void 0) { fmt = SmartFormatFlags.Default; }
            // TODO: Add more ; use formats
            var stringFormat = this.binding.type === Safe.VarType.Date ? 'dddd' : 'LLL';
            if (_.isString(fmt)) {
                stringFormat = fmt;
            }
            return moment(this.value).format(stringFormat);
        };
        SmartDate.prototype.valueOf = function () {
            return this.value.valueOf();
        };
        SmartDate.prototype.addDay = function (num) {
            if (num === void 0) { num = 1; }
            this.set(this.value.add(num, 'd'));
            return this;
        };
        SmartDate.prototype.addHour = function (num) {
            if (num === void 0) { num = 1; }
            this.set(this.value.add(num, 'h'));
            return this;
        };
        SmartDate.prototype.addMinute = function (num) {
            if (num === void 0) { num = 1; }
            this.set(this.value.add(num, 'm'));
            return this;
        };
        return SmartDate;
    }(SmartVar));
    Safe.SmartDate = SmartDate;
    var SmartNumber = (function (_super) {
        __extends(SmartNumber, _super);
        function SmartNumber(init, boundObj) {
            return _super.call(this, init, boundObj) || this;
        }
        SmartNumber.GetRandom = function (min, max) {
            if (min === void 0) { min = 0; }
            if (max === void 0) { max = 1000000000; }
            return new SmartNumber(_.random(min, max));
        };
        SmartNumber.prototype.preSet = function (newVal) {
            if (_.isNumber(newVal))
                return newVal;
            if (_.isString(newVal)) {
                var convert = parseFloat(newVal);
                // TODO: This could set the value to NaN, which is prolly what we want...
                //if (_.isNumber(convert)) {
                return convert;
                //}
            }
            throw new Safe.System.FoundationException("Attempt to set invalid number on bound value. " + this.binding.toString());
        };
        return SmartNumber;
    }(SmartVar));
    Safe.SmartNumber = SmartNumber;
    var SmartEnum = (function (_super) {
        __extends(SmartEnum, _super);
        function SmartEnum(enumType, init, boundObj) {
            var _this = _super.call(this, init, boundObj) || this;
            _this.enumType = enumType;
            // run the value thru assignment to convert to enum
            _this.set(_this.value);
            return _this;
        }
        SmartEnum.prototype.preSet = function (newVal) {
            var e = undefined;
            if (_.isNumber(newVal)) {
                if (!_.isUndefined(this.enumType[newVal])) {
                    e = newVal; // return number, not looked up string
                }
            }
            else if (_.isString(newVal)) {
                e = this.enumType[newVal]; // string lookup, may be undefined
            }
            if (!_.isUndefined(e)) {
                return e;
            }
            throw new Safe.System.FoundationException("Attempt to set illegal enum value. " + this.binding.toString());
        };
        SmartEnum.prototype.toString = function () {
            return this.enumType[this.value];
        };
        return SmartEnum;
    }(SmartVar));
    Safe.SmartEnum = SmartEnum;
    var SmartObj = (function (_super) {
        __extends(SmartObj, _super);
        function SmartObj(init, boundObj) {
            return _super.call(this, init, boundObj) || this;
        }
        SmartObj.prototype.preSet = function (newVal) {
            if (_.isObject(newVal)) {
                return newVal;
            }
            if (_.isString(newVal)) {
                return JSON.parse(newVal);
            }
            throw new Safe.System.FoundationException("Attempt to set invalid object on bound value. " + this.binding.toString());
        };
        SmartObj.prototype.getAt = function (path) {
            path;
            // let working = this.value;
            // let smartVar: SmartAny = null;
            // let parts = path.split('.');
            //
            // _.each(parts, part => {
            //     working = working[part];
            //     smartVar = (working instanceof SmartVar) ? working : SmartVar.BindTo(new NullBinding(part), working);
            //     // TODO: Something something something....
            // });
            throw new Safe.System.NotImplementedException();
            //return smartVar;
        };
        return SmartObj;
    }(SmartVar));
    Safe.SmartObj = SmartObj;
    var SmartString = (function (_super) {
        __extends(SmartString, _super);
        function SmartString(init, boundObj) {
            var _this = _super.call(this, init, boundObj) || this;
            _this.autoValidate = false;
            _this.pattern = /.*/;
            return _this;
        }
        SmartString.prototype.preSet = function (newVal) {
            var s = undefined;
            if (!(_.isObject(newVal) || _.isArray(newVal))) {
                s = newVal.toString();
            }
            if (s && this.autoValidate) {
                this.assertMatch();
            }
            if (!_.isUndefined(s)) {
                return s;
            }
            throw new Safe.System.FoundationException("Attempt to set invalid string on bound value. " + this.binding.toString());
        };
        Object.defineProperty(SmartString.prototype, "seemsLikeId", {
            //region helpers
            get: function () {
                // TODO: Use RegEx ??? Right now, cheat...
                if (this.hasValue) {
                    var v = this.value;
                    // reasonable length, containing dot, and no spaces
                    if (v.length > 8 && v.length < 128 && /\./.test(v)) {
                        return !/\s/.test(v);
                    }
                }
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SmartString.prototype, "isBlank", {
            get: function () {
                return this.isNullOrUndefined || /\S/.test(this.value);
            },
            enumerable: true,
            configurable: true
        });
        SmartString.prototype.contains = function (str) {
            return this.hasValue && this.value.indexOf(str) !== -1;
        };
        SmartString.prototype.match = function (regex) {
            if (regex === void 0) { regex = this.pattern; }
            return regex.test(this.value);
        };
        SmartString.prototype.assertMatch = function (regex) {
            if (regex === void 0) { regex = this.pattern; }
            if (!this.match(regex)) {
                throw new Safe.System.AssertionException();
            }
            return this;
        };
        return SmartString;
    }(SmartVar));
    Safe.SmartString = SmartString;
    var SmartArray = (function (_super) {
        __extends(SmartArray, _super);
        function SmartArray(init, boundObj) {
            var _this = _super.call(this, init, boundObj) || this;
            _this.separator = ' ';
            return _this;
        }
        SmartArray.prototype.preSet = function (newVal) {
            var a = null;
            if (_.isArray(newVal)) {
                a = ([].concat(newVal));
            }
            if (this.binding.type === Safe.VarType.Bag && _.isString(newVal)) {
                a = newVal.split(this.separator);
            }
            if (a) {
                if (this.binding.type === Safe.VarType.Bag) {
                    a = _.uniq(a);
                }
                return a;
            }
            throw new Safe.System.FoundationException("Attempt to set invalid array type. " + this.binding.toString());
        };
        SmartArray.prototype.toString = function () {
            if (this.isNullOrUndefined)
                return '';
            return this.binding.type === Safe.VarType.Bag ? this.value.join(this.separator) : this.valueOf().toString();
        };
        SmartArray.prototype.valueOf = function () {
            return this.value;
        };
        SmartArray.prototype.add = function (item) {
            var _this = this;
            this.setIf([]);
            var items = _.isArray(item) ? item : [item];
            _.each(items, function (item) { return _this.value.push(item); });
            if (this.binding.type === Safe.VarType.Bag) {
                this.set(_.uniq(this.value));
            }
            return this;
        };
        SmartArray.prototype.addIf = function (items, predicate) {
            if (predicate === void 0) { predicate = function (sa) { return sa.isUndefined; }; }
            return predicate(this) ? this.add(items) : this;
        };
        SmartArray.prototype.remove = function (item) {
            if (this.hasValue) {
                var items = _.isArray(item) ? item : [item];
                this.set(_.difference(this.value, items));
            }
            return this;
        };
        SmartArray.prototype.removeAll = function () {
            var _this = this;
            // don't set an undefined value to empty
            this.setIf([], function () { return _this.hasValue; });
            return this;
        };
        SmartArray.prototype.init = function (item) {
            return this.hasValue ? this : this.removeAll().add(item);
        };
        SmartArray.prototype.contains = function (item, all) {
            if (all === void 0) { all = true; }
            if (this.hasValue) {
                var items = _.isArray(item) ? item : [item];
                var diff = _.difference(this.value, items);
                if (diff.length) {
                    return (diff.length === items.length) ? false : !all;
                }
                else {
                    // no difference
                    return true;
                }
            }
            return false;
        };
        return SmartArray;
    }(SmartVar));
    Safe.SmartArray = SmartArray;
    var SmartRef = (function (_super) {
        __extends(SmartRef, _super);
        function SmartRef(init, boundObj) {
            var _this = _super.call(this, init, boundObj) || this;
            throw new Safe.System.NotImplementedException();
            return _this;
        }
        return SmartRef;
    }(SmartVar));
    Safe.SmartRef = SmartRef;
    //endregion
})(Safe || (Safe = {}));
//# sourceMappingURL=bbb_smartVar.js.map