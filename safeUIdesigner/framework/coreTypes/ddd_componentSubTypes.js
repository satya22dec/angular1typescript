var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    //region derived, specialty classes
    var Control = (function (_super) {
        __extends(Control, _super);
        function Control() {
            var _this = _super.call(this) || this;
            _this.inShowProp.setIf(true);
            return _this;
        }
        Control.prototype.$onInit = function () {
            _super.prototype.$onInit.call(this);
        };
        Control.prototype.$onDestroy = function () {
            _super.prototype.$onDestroy.call(this);
        };
        return Control;
    }(Safe.Component));
    //region bindings
    Control.exBindings = [
        new Safe.StaticBinding('className', Safe.VarType.Bag).useSmartVar(),
        new Safe.InputBinding('inStyle', Safe.VarType.Object).useSmartVar(),
        new Safe.InputBinding('inShow', Safe.VarType.Boolean, true).useSmartVar(),
    ];
    Safe.Control = Control;
    var InteractiveControl = (function (_super) {
        __extends(InteractiveControl, _super);
        function InteractiveControl() {
            var _this = _super.call(this) || this;
            _this.inDisabledProp.setIf(false);
            return _this;
        }
        //endregion
        InteractiveControl.prototype.fireEvent = function (type, associatedData) {
            this.onEventProp.invoke(new Safe.SafeEvent(this, type, associatedData));
        };
        return InteractiveControl;
    }(Control));
    //region bindings
    InteractiveControl.exBindings = [
        new Safe.InputBinding('inDisabled', Safe.VarType.Boolean, false).useSmartVar(),
        new Safe.CallbackBinding('onEvent').useSmartVar(),
    ];
    Safe.InteractiveControl = InteractiveControl;
    var NamedControl = (function (_super) {
        __extends(NamedControl, _super);
        function NamedControl() {
            var _this = _super.call(this) || this;
            _this.nameProp.setIf(_this.constructor.name);
            return _this;
        }
        return NamedControl;
    }(InteractiveControl));
    NamedControl.exBindings = [
        new Safe.StaticBinding('name').useSmartVar(),
    ];
    Safe.NamedControl = NamedControl;
    var ComplexControl = (function (_super) {
        __extends(ComplexControl, _super);
        function ComplexControl() {
            return _super.call(this) || this;
        }
        ComplexControl.prototype.$postLink = function () {
            _super.prototype.$postLink.call(this);
            // NOTE: Assert that a complex control gets input (NULL is fine)
            this.inPutProp.assert();
        };
        return ComplexControl;
    }(NamedControl));
    ComplexControl.exBindings = [
        new Safe.InputBinding('inPut', Safe.VarType.Any).useSmartVar(),
    ];
    Safe.ComplexControl = ComplexControl;
    var FormControl = (function (_super) {
        __extends(FormControl, _super);
        function FormControl() {
            var _this = _super.call(this) || this;
            _this.classNameProp.init(FormControl.defaultClasses);
            return _this;
        }
        return FormControl;
    }(NamedControl));
    FormControl.defaultClasses = ['form-control'];
    Safe.FormControl = FormControl;
    //endregion
})(Safe || (Safe = {}));
//# sourceMappingURL=ddd_componentSubTypes.js.map