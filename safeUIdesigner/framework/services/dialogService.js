var Safe;
(function (Safe) {
    var System;
    (function (System) {
        var DialogService = (function () {
            function DialogService(modalService, safeModule) {
                this.modalService = modalService;
                this.safeModule = safeModule;
                this.openInstances = [];
            }
            DialogService.prototype.popup = function (dialogComponent, bindingData, settings) {
                if (bindingData === void 0) { bindingData = {}; }
                if (settings === void 0) { settings = {}; }
                throw new System.NotImplementedException();
                // // Lookup the "real" component class for the one they passed, so variations work
                // let name = _.isString(dialogComponent) ? dialogComponent.replace('-', '') : dialogComponent.ngName;
                // dialogComponent = this.safeModule.componentMaps.product[name];
                //
                // let dialogSettings: bts.IModalSettings = {
                //     // user-defined settings
                //     windowTopClass: settings.windowTopClass,
                //     backdropClass: settings.backdropClass,
                //     openedClass: settings.openedClass,
                //     windowClass: settings.windowClass,
                // };
                // let options: ng.IComponentOptions = {
                //     controller: dialogComponent,
                //     controllerAs: '$ctrl',
                //     template: dialogComponent.template,
                //     transclude: dialogComponent.transclude,
                //     // TODO: bindings: xxx
                // };
                //
                // // TODO: Support two-way binding (on return?)
                // if (_.any(_.values(options.bindings), v => v === '&' || v === '=')) {
                //     throw new ApplicationException('Illegal use of callback or two-way binding in dialog-hosted component');
                // }
                //
                // // internal settings
                // dialogSettings.controller = DialogService.dialogInjections.concat(dialogComponent.$inject);
                // dialogSettings.controller.push(DialogService.dialogController);
                // dialogSettings.template = `<div id="Safe.System.Dialog">${dialogComponent.template}</div>`;
                // dialogSettings.resolve = { __S_componentClass: dialogComponent, __S_bindingData: bindingData };
                //
                // let instance = this.modalService.open(dialogSettings);
                //
                // // TODO: Return our own promise fulfilling type?
                //
                // this.openInstances.push(instance);
                //
                // instance.result.then(() => {
                //     let h = 0;
                // });
                // instance.result.then(() => {
                //     let i = 0;
                // });
                // instance.closed.then(() => {
                //     let j = 0;
                //     this.openInstances = _.without(this.openInstances, instance);
                // });
                // instance.result.catch(() => {
                //     let k = 0;
                // });
                //
                // return instance;
            };
            DialogService.dialogController = function (instance, componentClass, bindings) {
                var addlArgs = [];
                for (var _i = 3; _i < arguments.length; _i++) {
                    addlArgs[_i - 3] = arguments[_i];
                }
                var newComponent = Object.create(componentClass.prototype);
                // TODO: Check to see binding data matches component's bindings
                // assign into new object before ctor is called
                _.assign(newComponent, bindings);
                newComponent['__S_dialogInstance'] = instance; // private member
                var result = componentClass.apply(newComponent, addlArgs);
                return (result !== null && _.isObject(result)) ? result : newComponent;
            };
            return DialogService;
        }());
        DialogService.$inject = ['$uibModal', 'safeModule'];
        DialogService.dialogInjections = ['$uibModalInstance', '__S_componentClass', '__S_bindingData'];
        System.DialogService = DialogService;
        var DialogResult;
        (function (DialogResult) {
            DialogResult[DialogResult["Unknown"] = 0] = "Unknown";
            DialogResult[DialogResult["OK"] = 1] = "OK";
            DialogResult[DialogResult["Cancel"] = 2] = "Cancel";
            DialogResult[DialogResult["Close"] = 3] = "Close";
        })(DialogResult = System.DialogResult || (System.DialogResult = {}));
    })(System = Safe.System || (Safe.System = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=dialogService.js.map