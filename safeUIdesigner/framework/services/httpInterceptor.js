var Safe;
(function (Safe) {
    var System;
    (function (System) {
        /** This type's responsibility is simple: turn Http response errors into our Exception */
        var HttpInterceptor = (function () {
            function HttpInterceptor($q) {
                var _this = this;
                this.$q = $q;
                this.responseError = function (rejection) {
                    if ((rejection.status > Safe.HttpStatusCode.InternalServerError &&
                        rejection.status < Safe.HttpStatusCode.HttpVersionNotSupported) ||
                        rejection.status === Safe.HttpStatusCode.E_FAIL) {
                        throw new System.ServerUnavailableException(rejection);
                    }
                    if (rejection.status === Safe.HttpStatusCode.Unauthorized)
                        return _this.$q.reject(rejection);
                    if (rejection.status === Safe.HttpStatusCode.UnProcessableEntity) {
                        throw new System.ServerValidationException(rejection);
                    }
                    throw new System.HttpException(rejection);
                };
            }
            return HttpInterceptor;
        }());
        HttpInterceptor.$inject = ['$q'];
        System.HttpInterceptor = HttpInterceptor;
    })(System = Safe.System || (Safe.System = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=httpInterceptor.js.map