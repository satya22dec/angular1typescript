/**
 * Created by mtelb on 5/10/17.
 */
var Safe;
(function (Safe) {
    var System;
    (function (System) {
        var UI;
        (function (UI) {
            var ServerValidationExceptionViewer = (function () {
                function ServerValidationExceptionViewer(instance, exception) {
                    this.instance = instance;
                    this.exception = exception;
                    this.errors = [];
                    if (exception && exception.errors)
                        this.errors = exception.errors;
                }
                ServerValidationExceptionViewer.prototype.close = function () {
                    this.instance.close();
                };
                return ServerValidationExceptionViewer;
            }());
            ServerValidationExceptionViewer.$inject = ['$uibModalInstance', 'error'];
            ServerValidationExceptionViewer.template = "\n            <div class=\"modal-header\">\n                <h2 style=\"margin: 1em; text-align: center\">Following Validation Error(s) Occurred !</h2>\n                <hr/>\n            </div>\n            <div class=\"modal-body\">\n                <ul>\n                    <li ng-repeat=\"error in $ctrl.errors\">\n                        <p>{{ error.message | translate }}</p>\n                    </li>\n                </ul>\n            </div>\n            <div class=\"modal-footer\">\n                <button class=\"btn btn-primary\" ng-click=\"$ctrl.close()\">Close</button>\n            </div>\n";
            UI.ServerValidationExceptionViewer = ServerValidationExceptionViewer;
        })(UI = System.UI || (System.UI = {}));
    })(System = Safe.System || (Safe.System = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=serverValidationExceptionViewer.js.map