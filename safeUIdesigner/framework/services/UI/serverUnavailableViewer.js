/**
 * Created by mtelb on 5/10/17.
 */
var Safe;
(function (Safe) {
    var System;
    (function (System) {
        var UI;
        (function (UI) {
            var ServerUnavailableViewer = (function () {
                function ServerUnavailableViewer(instance, error) {
                    this.instance = instance;
                    this.error = error;
                }
                ServerUnavailableViewer.prototype.close = function () {
                    this.instance.close();
                };
                return ServerUnavailableViewer;
            }());
            ServerUnavailableViewer.$inject = ['$uibModalInstance', 'error'];
            ServerUnavailableViewer.template = "\n            <div class=\"modal-header\">\n                <h2 style=\"margin: 1em; text-align: center\">Nobody's home...<span style=\"font-family: 'Segoe UI Emoji', 'Segoe UI Symbol', sans-serif\">&#9785;</span></h2>\n                <hr/>\n            </div>\n            <div class=\"modal-body\">\n                <p>We're trying to contact the server, but it's unavailable. Please try refreshing your browser or logging out and back in. If the problem persists, contact your administrator.</p>\n            </div>\n            <div class=\"modal-footer\">\n                <button class=\"btn btn-primary\" ng-click=\"$ctrl.close()\">Close</button>\n            </div>\n";
            UI.ServerUnavailableViewer = ServerUnavailableViewer;
        })(UI = System.UI || (System.UI = {}));
    })(System = Safe.System || (Safe.System = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=serverUnavailableViewer.js.map