var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Product;
    (function (Product) {
        var Library;
        (function (Library) {
            var Table;
            (function (Table) {
                var Assert = Safe.System.Assert;
                var QuickTable = (function (_super) {
                    __extends(QuickTable, _super);
                    function QuickTable($transclude, $scope, $element) {
                        var _this = _super.call(this) || this;
                        _this.$transclude = $transclude;
                        _this.$scope = $scope;
                        _this.$element = $element;
                        // internal "pool" of data, managed by smart table Third-Party component
                        _this.displayed = null;
                        _this.classNameProp.set(QuickTable.defaultClasses);
                        _this.rowClassProp.set(['table-row']);
                        _this.displayed = _this.inPutProp.value;
                        var content = _this.$transclude();
                        _this.createHeader(content);
                        _this.createFooter(content);
                        _this.seal(QuickTable);
                        return _this;
                    }
                    QuickTable.prototype.$onInit = function () {
                        _super.prototype.$onInit.call(this);
                    };
                    QuickTable.prototype.createHeader = function (content) {
                        var _this = this;
                        var insertionPoint = this.$element.find('table thead tr');
                        var jColumns = content.filter(Column.ngName.dasherize());
                        jColumns.each(function (idx, el) {
                            var jElement = $(el);
                            var column = jElement.controller(Column.ngName);
                            Assert.hasValue(column);
                            _this.addChild(column);
                            insertionPoint.append($('<th>').append(jElement));
                        });
                    };
                    QuickTable.prototype.createFooter = function (content) {
                        var jDataPager = content.filter(Library.DataPager.ngName.dasherize());
                        if (jDataPager.length) {
                            var jFooter = $(QuickTable.footer);
                            var insertionPoint = jFooter.find('tr td');
                            insertionPoint.addClass('text-center table-row').append(jDataPager.children());
                            this.$element.find('tbody').after(jFooter);
                        }
                    };
                    QuickTable.prototype.$postLink = function () {
                        _super.prototype.$postLink.call(this);
                    };
                    return QuickTable;
                }(Safe.ComplexControl));
                QuickTable.defaultClasses = ['table', 'responsive-table'];
                QuickTable.ngName = 'safeQuickTable';
                QuickTable.$inject = ['$transclude', '$scope', '$element'];
                QuickTable.exBindings = [
                    new Safe.StaticBinding('rowClass', Safe.VarType.Bag).useSmartVar(),
                    //new InputBinding('inPerPage', VarType.Number, 10).useSmartVar(),
                    new Safe.InputBinding('inAuto', Safe.VarType.Boolean, false).useSmartVar(),
                ];
                QuickTable.transclude = true;
                QuickTable.template = "<table ng-class=\"$ctrl.classNameProp.forMarkup\" \n    st-table=\"$ctrl.displayed\" \n    st-safe-src=\"$ctrl.inPut\">\n    \n    <thead>\n        <tr>\n        </tr>\n    </thead>\n    <tbody>\n        <tr ng-class=\"$ctrl.rowClassProp.forMarkup\" \n        ng-click=\"$ctrl.fireEvent('Alert')\"\n        ng-repeat=\"row in $ctrl.displayed\"\n        safe-Data-Row>\n        </tr>\n    </tbody>\n </table>\n";
                QuickTable.footer = "<tfoot><tr><td></td></tr></tfoot>";
                Table.QuickTable = QuickTable;
                var Column = (function (_super) {
                    __extends(Column, _super);
                    function Column($transclude, $scope, $element) {
                        var _this = _super.call(this) || this;
                        _this.$transclude = $transclude;
                        _this.$scope = $scope;
                        _this.$element = $element;
                        _this.classNameProp.set(Column.defaultClasses);
                        _this.seal(Column);
                        return _this;
                    }
                    Column.prototype.$postLink = function () {
                        _super.prototype.$postLink.call(this);
                    };
                    return Column;
                }(Safe.NamedControl));
                Column.defaultClasses = ['table-head'];
                Column.ngName = "safeTableColumn";
                Column.$inject = ['$transclude', '$scope', '$element'];
                Column.transclude = true;
                Column.template = 
                // TODO: I HATE BINDING TO THE PARENT VALUES HERE. NOW THAT I FIGURED OUT TRANSCLUSION, FIX ASAP
                "<span ng-class=\"$ctrl.classNameProp.forMarkup\" \n    st-table=\"$ctrl.parent.displayed\"\n    st-safe-src=\"$ctrl.parent.inPut\"\n    st-sort=\"{{$ctrl.name}}\">{{$ctrl.headerText}}</span>";
                Column.exBindings = [
                    new Safe.StaticBinding('headerText').useSmartVar()
                ];
                Table.Column = Column;
                /** Internal use only - Directive used in table generation. Registered with Angular in module.ts */
                var SafeDataRow = (function () {
                    function SafeDataRow($compile) {
                        var _this = this;
                        this.$compile = $compile;
                        this.priority = 0;
                        this.restrict = 'A';
                        // TODO: Since this bad boy executes all the friggin time for every row, multiple times,
                        // TODO: look into caching the cell/markup JQuery objects for cloning. Dunno if this is a real problem yet.
                        // TODO: Put a lot of this into factory methods
                        this.link = {
                            pre: function (scope, instanceElement) {
                                var table = scope.$ctrl;
                                Assert.hasValue(table);
                                var rowData = scope.row;
                                _.each(table.getChildrenOfType(Column), function (column) {
                                    var name = column.nameProp.value;
                                    var cellValue = rowData[name];
                                    // new scope from my parent, non-isolated so bindings to $ctrl work right
                                    // also add context to scope to allow transcluded components to access them.
                                    var newScope = scope.$parent.$new();
                                    newScope.rowData = rowData;
                                    newScope.cellValue = rowData[name];
                                    column.$transclude(newScope, function (clone) {
                                        var newContent = clone.filter('*').length ?
                                            _this.$compile(clone)(newScope) :
                                            _this.$compile($("<span>{{cellValue}}</span>"))(newScope);
                                        instanceElement.append($('<td>').addClass('cell-table').append(newContent));
                                    });
                                });
                            }
                        };
                        if (!(this instanceof SafeDataRow))
                            return new SafeDataRow($compile);
                    }
                    return SafeDataRow;
                }());
                SafeDataRow.$inject = ['$compile'];
                Table.SafeDataRow = SafeDataRow;
            })(Table = Library.Table || (Library.Table = {}));
        })(Library = Product.Library || (Product.Library = {}));
    })(Product = Safe.Product || (Safe.Product = {}));
})(Safe || (Safe = {}));
/*
 // TODO: Any reason to walk transcluded components? Resurrect this code in some TBD form...

 //region --spelunk components
 // fixup new/transcluded components with data if needed
 // let components =
 //     newContent.filter((idx, el) => el.nodeType === Node.ELEMENT_NODE && el.tagName.toUpperCase().indexOf('SAFE-') === 0);
 //
 // components.each((idx, el) => {
 //     let jElement = $(el);
 //     let tagName = el.tagName;
 //     let ngName = tagName.dedasherize();
 //     let control: Control = jElement.controller(ngName);
 //     Assert.hasValue(control);
 //
 //     // control.inPutProp.setIf(rowData);
 //     // control.ioValueProp.setIf(cellValue);
 // });
 //endregion

 */ 
//# sourceMappingURL=table.js.map