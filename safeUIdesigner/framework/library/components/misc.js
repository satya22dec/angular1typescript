var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Product;
    (function (Product) {
        var Library;
        (function (Library) {
            var ApplicationException = Safe.System.ApplicationException;
            var Text = (function (_super) {
                __extends(Text, _super);
                function Text($scope, $element) {
                    var _this = _super.call(this) || this;
                    _this.$scope = $scope;
                    _this.$element = $element;
                    return _this;
                }
                Text.prototype.$postLink = function () {
                    _super.prototype.$postLink.call(this);
                };
                return Text;
            }(Safe.InteractiveControl));
            Text.ngName = 'safeText';
            Text.$inject = ['$scope', '$element'];
            Text.template = "\n<span id=\"{{$ctrl.id}}\" \n    ng-class=\"$ctrl.classNameProp.forMarkup\"\n    ng-style=\"$ctrl.inStyle\">{{$ctrl.ioValueProp.forMarkup}}</span>\n";
            Library.Text = Text;
            var DataPager = (function (_super) {
                __extends(DataPager, _super);
                function DataPager($scope) {
                    var _this = _super.call(this) || this;
                    _this.$scope = $scope;
                    _this.classNameProp.setIf(['custom-angularstrap', 'pagination']);
                    _this.$scope.$on('pagination:startLoad', function () {
                        _this.fireEvent(Safe.SafeEventType.Load & Safe.SafeEventType.Before);
                    });
                    _this.$scope.$on('pagination:loadPage', function () {
                        _this.fireEvent(Safe.SafeEventType.Load & Safe.SafeEventType.After);
                    });
                    _this.$scope.$on('pagination:error', function (event, status, config) {
                        throw new ApplicationException("bgf-pagination had an error. Here's maybe some help: Event: " + JSON.stringify(event) + "; Status: " + JSON.stringify(status) + "; Config: " + JSON.stringify(config));
                    });
                    _this.seal(DataPager);
                    return _this;
                }
                return DataPager;
            }(Safe.InteractiveControl));
            DataPager.ngName = 'safeDataPager';
            DataPager.$inject = ['$scope'];
            DataPager.exBindings = [
                new Safe.InputBinding('inPerPage', Safe.VarType.Number, 10).useSmartVar(),
            ];
            DataPager.template = "<bgf-Pagination\n    ng-class=\"$ctrl.classNameProp.forMarkup\"\n    collection=\"$ctrl.ioValue\"\n    url=\"$ctrl.inPut\" \n    per-page=\"$ctrl.inPerPage\">\n</bgf-Pagination>\n";
            Library.DataPager = DataPager;
        })(Library = Product.Library || (Product.Library = {}));
    })(Product = Safe.Product || (Safe.Product = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=misc.js.map