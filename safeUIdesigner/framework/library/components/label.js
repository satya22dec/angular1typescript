var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Product;
    (function (Product) {
        var Library;
        (function (Library) {
            var SafeLabel = (function (_super) {
                __extends(SafeLabel, _super);
                function SafeLabel() {
                    var _this = _super.call(this) || this;
                    _this.styles = {
                        Top: 'display: block',
                        Left: 'display: inline-block; float: left; width: 15em; margin-right: 5px',
                        Bottom: '',
                        Right: 'display: inline-block; float: right; width: 15em; margin-right: 5px'
                    };
                    _this.positionProp.setIf(Safe.Position.Top);
                    _this.classNameProp.init('form-label');
                    _this.chosenStyle = _this.styles[_this.positionProp.toString()];
                    _this.seal(SafeLabel);
                    return _this;
                }
                return SafeLabel;
            }(Safe.Control));
            //region bindings
            SafeLabel.ngName = 'safeLabel';
            SafeLabel.transclude = true;
            /* for html only
            private position: Position;
            private forId: string;
            */
            SafeLabel.exBindings = [
                new Safe.StaticBinding('position', Safe.Position, Safe.Position.Top).useSmartVar(),
                new Safe.StaticBinding('forId').useSmartVar()
            ];
            //endregion
            SafeLabel.template = "<label for=\"{{$ctrl.forId}}\" ng-show=\"$ctrl.inShow\" ng-style=\"$ctrl.chosenStyle\">{{$ctrl.ioValue}}\n                <ng-transclude>NOTHING TO LABEL!</ng-transclude>\n            </label>\n            ";
            Library.SafeLabel = SafeLabel;
        })(Library = Product.Library || (Product.Library = {}));
    })(Product = Safe.Product || (Safe.Product = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=label.js.map