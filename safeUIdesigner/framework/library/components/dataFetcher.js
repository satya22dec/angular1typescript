var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Product;
    (function (Product) {
        var Library;
        (function (Library) {
            var DataFetcher = (function (_super) {
                __extends(DataFetcher, _super);
                function DataFetcher($http) {
                    var _this = _super.call(this) || this;
                    _this.$http = $http;
                    return _this;
                }
                DataFetcher.prototype.$postLink = function () {
                    var _this = this;
                    _super.prototype.$postLink.call(this);
                    this.$http.get(this.inPutProp.value).then(function (result) {
                        var data = result.data;
                        _this.ioValueProp.set(data);
                    });
                };
                return DataFetcher;
            }(Safe.ComplexControl));
            DataFetcher.$inject = ['$http'];
            DataFetcher.ngName = 'safeFetch';
            DataFetcher.template = '<span style="display:none;visibility:hidden"></span>';
            Library.DataFetcher = DataFetcher;
        })(Library = Product.Library || (Product.Library = {}));
    })(Product = Safe.Product || (Safe.Product = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=dataFetcher.js.map