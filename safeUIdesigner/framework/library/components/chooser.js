var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Product;
    (function (Product) {
        var Library;
        (function (Library) {
            var Chooser = (function (_super) {
                __extends(Chooser, _super);
                function Chooser() {
                    var _this = _super.call(this) || this;
                    throw new Safe.System.NotImplementedException();
                    return _this;
                }
                return Chooser;
            }(Safe.InteractiveControl));
            Chooser.ngName = 'safeChooser';
            Chooser.template = "\n            <safe-Radio-Buttons id=\"{{$ctrl.id.value}}\" out-Collection=\"$ctrl.buttons\">\n                \n            </safe-Radio-Buttons>\n            ";
            Library.Chooser = Chooser;
        })(Library = Product.Library || (Product.Library = {}));
    })(Product = Safe.Product || (Safe.Product = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=chooser.js.map